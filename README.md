# DQAME_firmware

# Getting Project setup for platformio (with GCC compiler) and unit testing with ceedling
This project can be compiled using arm gcc in platformio or XC32 in mplabx. Both options should be feature complete. If new source files are added in one IDE, the include settings on the other IDE will likely have to be modified.

## platformio
Download and install [visual studio code](https://code.visualstudio.com/)

Install the [platformio extension](https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide)

Launch platformio in vs-code and navigate to "Platforms" Choose advanced installation and write "git+https://gitlab.cern.ch/christem/platformio-atsame" and then install.
This installs a custom platform that adds CMSIS support to our ATSAME processor.


## ceedling and gcc (mingw)
To install ceedling, ruby is required. Ruby can be installed on windows from the following [link](https://rubyinstaller.org/downloads/). Just download and install the lates x64 bit installer (without Devkit). 

Once ruby is installed, ceedling can be installed using the linked [instruction](http://www.throwtheswitch.org/ceedling).
To run ceedling, gcc must be installed and added to the environment path. On windows, gcc can be install using [mingw](https://sourceforge.net/projects/mingw/files/Installer/mingw-get-setup.exe/download)

## Unit testing
To run a unit test type the following in PlatformIO CLI (make sure the working directory is firmware):
ceedling test:TEST_UNIT
Where TEST_UNIT corresponds to the name of the unit test to be executed, e.g
ceedling test:test_circular_buffer
