#ifndef DQAME_V2_0_BSP_H    /* Guard against multiple inclusion */
#define DQAME_V2_0_BSP_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "peripheral/port/plib_port.h"

/*** Macros for LED_5G pin ***/
#define LED_5G_Set()              GPIO_PA00_Set()           
#define LED_5G_Clear()            GPIO_PA00_Clear()         
#define LED_5G_Toggle()           GPIO_PA00_Toggle()        
#define LED_5G_OutputEnable()     GPIO_PA00_OutputEnable()  
#define LED_5G_InputEnable()      GPIO_PA00_InputEnable()   
#define LED_5G_Get()              GPIO_PA00_Get()           
#define LED_5G_PIN                GPIO_PA00_PIN             
                                  
/*** Macros for LED_5R pin ***/   
#define LED_5R_Set()              GPIO_PA01_Set()           
#define LED_5R_Clear()            GPIO_PA01_Clear()         
#define LED_5R_Toggle()           GPIO_PA01_Toggle()        
#define LED_5R_OutputEnable()     GPIO_PA01_OutputEnable()  
#define LED_5R_InputEnable()      GPIO_PA01_InputEnable()   
#define LED_5R_Get()              GPIO_PA01_Get()           
#define LED_5R_PIN                GPIO_PA01_PIN             
                                  
/*** Macros for LED_6 pin ***/    
#define LED_6_Set()               GPIO_PC02_Set()           
#define LED_6_Clear()             GPIO_PC02_Clear()         
#define LED_6_Toggle()            GPIO_PC02_Toggle()        
#define LED_6_OutputEnable()      GPIO_PC02_OutputEnable()  
#define LED_6_InputEnable()       GPIO_PC02_InputEnable()   
#define LED_6_Get()               GPIO_PC02_Get()           
#define LED_6_PIN                 GPIO_PC02_PIN             
                                  
/*** Macros for LED_7 pin ***/    
#define LED_7_Set()               GPIO_PC03_Set()           
#define LED_7_Clear()             GPIO_PC03_Clear()         
#define LED_7_Toggle()            GPIO_PC03_Toggle()        
#define LED_7_OutputEnable()      GPIO_PC03_OutputEnable()  
#define LED_7_InputEnable()       GPIO_PC03_InputEnable()   
#define LED_7_Get()               GPIO_PC03_Get()           
#define LED_7_PIN                 GPIO_PC03_PIN             
                                  
/*** Macros for LED_8 pin ***/    
#define LED_8_Set()               GPIO_PA02_Set()           
#define LED_8_Clear()             GPIO_PA02_Clear()         
#define LED_8_Toggle()            GPIO_PA02_Toggle()        
#define LED_8_OutputEnable()      GPIO_PA02_OutputEnable()  
#define LED_8_InputEnable()       GPIO_PA02_InputEnable()   
#define LED_8_Get()               GPIO_PA02_Get()           
#define LED_8_PIN                 GPIO_PA02_PIN             
                                  
/*** Macros for LED_9 pin ***/    
#define LED_9_Set()               GPIO_PA03_Set()           
#define LED_9_Clear()             GPIO_PA03_Clear()         
#define LED_9_Toggle()            GPIO_PA03_Toggle()        
#define LED_9_OutputEnable()      GPIO_PA03_OutputEnable()  
#define LED_9_InputEnable()       GPIO_PA03_InputEnable()   
#define LED_9_Get()               GPIO_PA03_Get()           
#define LED_9_PIN                 GPIO_PA03_PIN             
                                  
/*** Macros for LED_10 pin ***/   
#define LED_10_Set()              GPIO_PB04_Set()           
#define LED_10_Clear()            GPIO_PB04_Clear()         
#define LED_10_Toggle()           GPIO_PB04_Toggle()        
#define LED_10_OutputEnable()     GPIO_PB04_OutputEnable()  
#define LED_10_InputEnable()      GPIO_PB04_InputEnable()   
#define LED_10_Get()              GPIO_PB04_Get()           
#define LED_10_PIN                GPIO_PB04_PIN             
                                  
/*** Macros for LED_11 pin ***/   
#define LED_11_Set()              GPIO_PB05_Set()           
#define LED_11_Clear()            GPIO_PB05_Clear()         
#define LED_11_Toggle()           GPIO_PB05_Toggle()        
#define LED_11_OutputEnable()     GPIO_PB05_OutputEnable()  
#define LED_11_InputEnable()      GPIO_PB05_InputEnable()   
#define LED_11_Get()              GPIO_PB05_Get()           
#define LED_11_PIN                GPIO_PB05_PIN             
                                  
/*** Macros for LED_12 pin ***/   
#define LED_12_Set()              GPIO_PD00_Set()           
#define LED_12_Clear()            GPIO_PD00_Clear()         
#define LED_12_Toggle()           GPIO_PD00_Toggle()        
#define LED_12_OutputEnable()     GPIO_PD00_OutputEnable()  
#define LED_12_InputEnable()      GPIO_PD00_InputEnable()   
#define LED_12_Get()              GPIO_PD00_Get()           
#define LED_12_PIN                GPIO_PD00_PIN             
                                  
/*** Macros for GPIO_1 pin ***/   
#define GPIO_1_Set()              GPIO_PC05_Set()           
#define GPIO_1_Clear()            GPIO_PC05_Clear()         
#define GPIO_1_Toggle()           GPIO_PC05_Toggle()        
#define GPIO_1_OutputEnable()     GPIO_PC05_OutputEnable()  
#define GPIO_1_InputEnable()      GPIO_PC05_InputEnable()   
#define GPIO_1_Get()              GPIO_PC05_Get()           
#define GPIO_1_PIN                GPIO_PC05_PIN             
                                  
/*** Macros for GPIO_2 pin ***/   
#define GPIO_2_Set()              GPIO_PC06_Set()           
#define GPIO_2_Clear()            GPIO_PC06_Clear()         
#define GPIO_2_Toggle()           GPIO_PC06_Toggle()        
#define GPIO_2_OutputEnable()     GPIO_PC06_OutputEnable()  
#define GPIO_2_InputEnable()      GPIO_PC06_InputEnable()   
#define GPIO_2_Get()              GPIO_PC06_Get()           
#define GPIO_2_PIN                GPIO_PC06_PIN             
                                  
/*** Macros for LEMO pin ***/     
#define LEMO_Set()                GPIO_PC07_Set()           
#define LEMO_Clear()              GPIO_PC07_Clear()         
#define LEMO_Toggle()             GPIO_PC07_Toggle()        
#define LEMO_OutputEnable()       GPIO_PC07_OutputEnable()  
#define LEMO_InputEnable()        GPIO_PC07_InputEnable()   
#define LEMO_Get()                GPIO_PC07_Get()           
#define LEMO_PIN                  GPIO_PC07_PIN                        
                                  
/*** Macros for LED_1 pin ***/    
#define LED_1_Set()               GPIO_PA27_Set()           
#define LED_1_Clear()             GPIO_PA27_Clear()         
#define LED_1_Toggle()            GPIO_PA27_Toggle()        
#define LED_1_OutputEnable()      GPIO_PA27_OutputEnable()  
#define LED_1_InputEnable()       GPIO_PA27_InputEnable()   
#define LED_1_Get()               GPIO_PA27_Get()           
#define LED_1_PIN                 GPIO_PA27_PIN             
                                  
/*** Macros for LED_2 pin ***/    
#define LED_2_Set()               GPIO_PC30_Set()           
#define LED_2_Clear()             GPIO_PC30_Clear()         
#define LED_2_Toggle()            GPIO_PC30_Toggle()        
#define LED_2_OutputEnable()      GPIO_PC30_OutputEnable()  
#define LED_2_InputEnable()       GPIO_PC30_InputEnable()   
#define LED_2_Get()               GPIO_PC30_Get()           
#define LED_2_PIN                 GPIO_PC30_PIN

/*** Macros for PHY RESET pin ***/    
#define PHY_Set()               GPIO_PC10_Set()           
#define PHY_Clear()             GPIO_PC10_Clear()         
#define PHY_Toggle()            GPIO_PC10_Toggle()        
#define PHY_OutputEnable()      GPIO_PC10_OutputEnable()  
#define PHY_InputEnable()       GPIO_PC10_InputEnable()   
#define PHY_Get()               GPIO_PC10_Get()           
#define PHY_PIN                 GPIO_PC10_PIN

#ifdef	__cplusplus
}
#endif

#endif /* DQAME_V2_0_BSP_H */

