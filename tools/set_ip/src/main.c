/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdio.h>
#include <string.h>
#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
//#include "DQAME_V2_0_bsp.h"


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main(void)
{
    uint32_t data[128];
    uint32_t addr = 0x00804000;
    bool status = true;


    /* Initialize all modules */
    SYS_Initialize(NULL);
    memset(data,0,128*sizeof(uint32_t));

    //Read User page Data
    NVMCTRL_Read(data, 512, addr);

    char ip[] = "192.168.1.19";
    uint8_t ipWithPadding[16];
    memset(ipWithPadding, 0xFF, 16);

    uint8_t len = strlen(ip);
    
    memcpy(ipWithPadding, ip, len+1);
    
    char macAddr[] = "00:04:2E:1C:A0:0B";
    uint8_t macAddrPadding[32];
    memset(macAddrPadding, 0xFF, 32);
    memcpy(macAddrPadding, macAddr, 18);

    //Set ip and mac info in user page
    memcpy(&data[8], ipWithPadding, 16);
    memcpy(&data[16], macAddrPadding, 32);

    //Clear Page
    NVMCTRL_REGS->NVMCTRL_ADDR = addr;
    NVMCTRL_REGS->NVMCTRL_CTRLB = NVMCTRL_CTRLB_CMD_EP | NVMCTRL_CTRLB_CMDEX_KEY;

    //Write page:
    for(uint32_t i = 0; i<32;i++)
    {
      status &= NVMCTRL_QuadWordWrite((uint32_t*)&data[0+4*i], addr+0x10*i);
    }

    if (status == true)
    {
        //LED_1_Toggle();
    }
    else
    {
        //LED_2_Toggle();
    }
    while (true)
    {

    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE);
}


/*******************************************************************************
 End of File
 */

