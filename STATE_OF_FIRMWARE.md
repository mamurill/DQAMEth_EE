# State of Firmware

Current firmware is designed for version 0.4 of the protocol definition. None of the changes for 0.5 have been implemented

## TODOs

- Control traffic (where every packet is acknowledged) needs to be implemented.
- For some reason I can not get the project to compile using hardware floating point. Thus the current implementations uses software floating point. We only have limited floating point calculations, so it is not a big problem, however it is not ideal. The relevant code/configuration to look into is: `-llibarm_cortexM4l_math` and `-llibarm_cortexM4lf_mat` in `platformio.ini` as well as `"-mfloat-abi=soft"` in `post_extra_script.py`.
- `callocs` needs to be protected and checked if they are successful.
- `_destroy` functions need to be implemented properly (i.e. make sure they actually free all memory and protected against freeing already freed structured).
- Currently the firmware starts streaming data as soon as it has obtained a connection with the FEC. However, to remain compliant with the server behavior, the firmware should not start streaming data until synchronization has been achieved. Otherwise, the first many thousand packets will contain incorrect time stamps.
- Not all pins defined in "DQAME_V2_0_bsp.h" have been initialized in harmony.
- Recommended to make equivalent DQAME_V2_0_bsp.h" files for other boards and revisions.
- Thus far PTP has not been configured for Two Way Time Transfers. (i.e. only timestamp T1 and T2 are used to infer the offset. T3 and T4 are not used). This should be rectified. The relevant function to modify is `static void run_PID(struct PtpMetaData *ptpData)` (found in `ptp.c`). If in doubt, my master thesis explains how these timestamps should be used.
- We need a way to timestamp data acquired by the host device quickly. I recommend you choose an approach where you require the host device to set a `DATA_RDY` pin high as soon as it samples new data. Based on this pin, the firmware can enter an interrupt where the timestamp is saved.
- Leap seconds need to be accounted for.
- `tsu.c` relies on `PLL_PERIOD_NS` to accurately perform the timing arithmetic. However, this value is set equal to "10" and does not match the current clock period. This MUST be updated to allow for proper synchronization.


## Future Recommendations
- Implement commands to configure device (e.g. set MAC address, UID, IP, port, PID parameters, etc)
- Ability to perform remote firmware updates. Harmony has a nice sample application: https://microchip-mplab-harmony.github.io/bootloader_apps_ethernet/apps/udp_bootloader/readme.html
- In my opinion it is a really good idea to use Protobuf, if feasible. Current work can be found here: https://gitlab.cern.ch/christem/dqame_firmware/-/tree/protobuf. I think it would be valuable to throw engineering resources at optimizing the library. Even if the use protobuf will impose hard limits on our maximum achievable throughput, the flexibility it allows for might make it worth it. 
- While I have not used unit testing as much as I had hoped, I highly recommend you try to slowly make it use of it during future development.
- Re-implement code to generate PPS output on a pin. This is necessary to make high precision performance evaluations of the synchronization. Existing code for this can be found for this in https://gitlab.cern.ch/christem/dqame_ptp2/. Specifically commit 48104a40 and dbb4a808 might be of interest. The code to generate pulses relies on the CPU's TSU COMP Registers, interrupts, timers and event system. The basic steps for pulse generations are (in this case 1 second pulses):
    - The TSU should be configured to toggle a pin (through the event system) every time the Time is equal to the value in the COMP registers. It should also be configured to start an ISR
    - The TSU's COMP register are initialized with an initial time value that will occur sometime in the immediate future.
    - Once this value is reached the pin is automatically toggled. In the ISR the TSU's COMP register should be incremented by 1 second. This will ensure that the cycle will repeat itself the next second.
    - Additional code can be added (through a secondary timer) to facilitate short pulses (instead of just toggling between high and low voltage every second). This is shown in the linked Repository
