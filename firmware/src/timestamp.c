#include "timestamp.h"

/*******************************************************************
 * NAME :            time_stamp_subtract(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest)
 *
 * DESCRIPTION :     Subtracts two struct PTP_TIMESTAMP such that  dest = sourceA-sourceB
 *
 * INPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *sourceA            Timestamp
 *           struct PTP_TIMESTAMP               *sourceB            Timestamp to be subtracted from sourceA
 *           struct PTP_TIMESTAMP               *dest               Location to store the difference between sourceA and sourceB
 * OUTPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *dest
 */
void time_stamp_subtract(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest)
{
    int64_t seconds = ((int64_t) sourceA->secondsField) - ((int64_t) sourceB->secondsField);
    int32_t nanoseconds = ((int32_t) sourceA->nanosecondsField) - ((int32_t) sourceB->nanosecondsField);

    if (seconds < 0)
    {
        if (nanoseconds > 0)
        {
            seconds += 1;
            nanoseconds = BILLION - nanoseconds;
            seconds = -seconds;
        }
        else
        {
            seconds = -seconds;
            nanoseconds = -nanoseconds;
        }
        dest->subtract = true;
    }
    else if (seconds == 0)
    {
        dest->subtract = false;
        if (nanoseconds < 0)
        {
            nanoseconds = abs(nanoseconds);
            dest->subtract = true;
        }
    }
    else
    {
        if (nanoseconds < 0)
        {
            seconds -= 1;
            nanoseconds += BILLION;
        }
        dest->subtract = false;
    }

    dest->secondsField = (uint32_t) seconds;
    dest->nanosecondsField = (uint32_t) nanoseconds;
}

/*******************************************************************
 * NAME :            time_stamp_add(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest)
 *
 * DESCRIPTION :     Adds two struct PTP_TIMESTAMP such that  dest = sourceA+sourceB
 *
 * INPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *sourceA            Timestamp
 *           struct PTP_TIMESTAMP               *sourceB            Timestamp to be added to sourceA
 *           struct PTP_TIMESTAMP               *dest               Location to store the sum of sourceA and sourceB
 * OUTPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *dest
 */
void time_stamp_add(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest)
{
    if (!sourceA->subtract && sourceB->subtract)
    {
        time_stamp_subtract(sourceA, sourceB, dest);
    }
    else if (sourceA->subtract && !sourceB->subtract)
    {
        time_stamp_subtract(sourceA, sourceB, dest);
        dest->subtract = !dest->subtract;
    }
    else
    {
        int64_t seconds = ((int64_t) sourceA->secondsField) + ((int64_t) sourceB->secondsField);
        int32_t nanoseconds = ((int32_t) sourceA->nanosecondsField) + ((int32_t) sourceB->nanosecondsField);

        if (BILLION <= nanoseconds)
        {
            seconds++;
            nanoseconds = nanoseconds - BILLION;
        }

        dest->secondsField = (uint64_t) seconds;
        dest->nanosecondsField = (uint32_t) nanoseconds;
        dest->subtract = sourceA->subtract;
    }

}

int64_t time_stamp_to_ns(struct PTP_TIMESTAMP sourceA)
{
    int64_t temp = sourceA.nanosecondsField;
    temp += BILLION * sourceA.secondsField;
    if(sourceA.subtract)
    {
        temp = -temp;
    }
    return temp;
}