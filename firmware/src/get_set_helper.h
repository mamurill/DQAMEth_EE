#ifndef GET_SET_HELPER_H /* Guard against multiple inclusion */
#define GET_SET_HELPER_H

#include <stdint.h>
#include "DQAME_V2_0_bsp.h"
#include "byte_swap.h"

inline __attribute__((always_inline)) void set_byte(uint8_t *dst, uint8_t data)
{
    *dst = data;
}

inline __attribute__((always_inline)) void set_2byte(uint16_t *dst, uint16_t data, bool bigEndian)
{
    if (bigEndian)
    {
        bswap16(data, dst);
    }
    else
    {
        *dst = data;
    }
}

inline __attribute__((always_inline)) void set_4byte(uint32_t *dst, uint32_t data, bool bigEndian)
{
    if (bigEndian)
    {
        bswap32(data, dst);
    }
    else
    {
        *dst = data;
    }
}

inline __attribute__((always_inline)) void set_8byte(uint64_t *dst, uint64_t data, bool bigEndian)
{
    if (bigEndian)
    {
        bswap64(data, dst);
    }
    else
    {
        *dst = data;
    }
}

inline __attribute__((always_inline)) uint8_t get_byte(uint8_t src_data)
{
    return src_data;
}

inline __attribute__((always_inline)) uint16_t get_2byte(uint16_t src_data, bool bigEndian)
{
    if(bigEndian)
    {
        uint16_t tmp;
        bswap16(src_data, &tmp);
        return tmp;
    }
    else
    {
        return src_data;
    }
}

inline __attribute__((always_inline)) uint32_t get_4byte(uint32_t src_data, bool bigEndian)
{
    if(bigEndian)
    {
        uint32_t tmp;
        bswap32(src_data, &tmp);
        return tmp;
    }
    else
    {
        return src_data;
    }
}

inline __attribute__((always_inline)) uint64_t get_8byte(uint64_t src_data, bool bigEndian)
{
    if(bigEndian)
    {
        uint64_t tmp;
        bswap64(src_data, &tmp);
        return tmp;
    }
    else
    {
        return src_data;
    }
}
#endif