#ifndef AGENT_EXAMPLE1_H
#define AGENT_EXAMPLE1_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"
#include "byte_swap.h"
#include "agent_control.h"
#include "compile_options.h"
#include "agent_template.h"
#include "tsu.h"
#include "DQAME_V2_0_bsp.h"

struct agentExampleObj
{
    struct agentBaseObj base;
};

struct agentExampleObj *create_example_agent(uint32_t maxFrameBytes, uint16_t port);
void destroy_example_agent(struct agentExampleObj *agent);
void start_example_agent(struct agentExampleObj* agent);
void run_example_agent(struct agentExampleObj* agent);

#endif // AGENT_EXAMPLE1_H
