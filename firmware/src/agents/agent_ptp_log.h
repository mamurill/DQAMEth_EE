#ifndef AGENT_PTP_LOG_H
#define AGENT_PTP_LOG_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"
#include "byte_swap.h"
#include "agent_control.h"
#include "compile_options.h"
#include "agents/agent_template.h"
//#include "data_generator.h"
#include "tsu.h"
#include "DQAME_V2_0_bsp.h"

struct agentPtpLogObj
{
    struct agentBaseObj base;
};

#pragma pack(1)
struct __attribute__((packed)) AgentPtpLogAcquisition
{
    uint32_t t1s;
    uint32_t t1ns;

    uint32_t t2s;
    uint32_t t2ns;

    uint32_t t3s;
    uint32_t t3ns;

    uint32_t t4s;
    uint32_t t4ns;

    uint32_t T2;
    uint32_t T4;

    double offsetEst;
    double desiredRate;
    double setRate;
};
#pragma pack()

struct AgentPtpLogAcquisition global_ptp_data;
bool ptpDataReady;

struct agentPtpLogObj *create_ptp_log_agent(uint32_t maxTxBytes, uint16_t port);
void destroy_ptp_log_agent(struct agentPtpLogObj* ptr);
void start_ptp_log_agent(struct agentPtpLogObj* agent);
void run_ptp_log_agent(struct agentPtpLogObj* agent);

#endif // AGENT_EXAMPLE1_H
