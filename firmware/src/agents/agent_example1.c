#include "agent_example1.h"
#include "agent_control.h"
#include "LUTS.h"

#pragma pack(1)
struct __attribute__((packed)) ExampleAgentAcquisition
{
    uint32_t UP1;
    uint32_t UP2;
    uint32_t UP3;
    uint32_t UP4;
    uint32_t U_BB;
    uint32_t Circuit_current_A;
    uint32_t Diff_AP1;
    uint32_t Diff_AP2;
    uint32_t Diff_AP12;
    uint32_t logics;
    uint32_t ActiveThreshold_comp_0;
    uint32_t ActiveThreshold_comp_1;
    uint32_t ActiveDiscriminator_comp_0;
    uint32_t ActiveDiscriminator_comp_1;
};
#pragma pack()

uint16_t count1 = 0;
uint16_t count2 = 16383;
//Generate sine wave
inline __attribute__((always_inline)) void UP1_gen(uint32_t *dest)
{
    *dest = sine[count1++];
}

//Generate sine wave 180 degress out of phase
inline __attribute__((always_inline)) void UP2_gen(uint32_t *dest)
{
    *dest = sine[count2++];
}

//Generate sine wave
void UP3_gen(uint32_t *dest)
{
    static uint16_t count = 0;
    *dest = sine[count++];
}

//Generate sine wave 180 degress out of phase
void UP4_gen(uint32_t *dest)
{
    static uint16_t count = 16383;
    *dest = sine[count++];
}

//Generate cosine wave
void U_BB_gen(uint32_t *dest)
{
    static uint16_t count = 8192;
    *dest = sine[count++];
}

//Generate cosine wave 180 degrees out of phase
void Circuit_current_A_gen(uint32_t *dest)
{
    static uint16_t count = 24575;
    *dest = sine[count++];
}

//Generate cosine wave
void Diff_AP2_gen(uint32_t *dest)
{
    static uint16_t count = 8192;
    *dest = sine[count++];
}

//Generate cosine wave 180 degree
void Diff_AP1_gen(uint32_t *dest)
{
    static uint16_t count = 24575;
    *dest = sine[count++];
}

//Generate cosine wave 180 degrees
void Diff_AP12_gen(uint32_t *dest)
{
    static uint16_t count = 24575;
    *dest = sine[count++];
}

//Increment
void logics_gen(uint32_t *dest)
{
    static uint16_t k = 0;
    k++;
    *dest = k;
}

//Decrement
void ActiveThreshold_comp_0_gen(uint32_t *dest)
{
    static uint16_t k = 0;
    k--;
    *dest = k;
}

//Triangle
void ActiveDiscriminator_comp_0_gen(uint32_t *dest)
{
    static uint8_t k = 0;
    *dest = triangle[k++];
}

//Triangle with double amplitude
void ActiveThreshold_comp_1_gen(uint32_t *dest)
{
    static uint8_t k = 0;
    *dest = triangle[k++] << 1;
}

//Toggle between 1 and 0
void ActiveDiscriminator_comp_1_gen(uint32_t *dest)
{
    static uint32_t k = 1;
    k = 1 - k;
    *dest = k;
}

//Constant Value
void Trigger_Lines_gen(uint32_t *dest)
{
    static uint32_t k = 0x89ABCDEF;
    *dest = k;
}

inline __attribute__((always_inline)) void get_stream_data(struct ExampleAgentAcquisition *data)
{
    UP1_gen(&data->UP1);
    UP2_gen(&data->UP2);
    UP3_gen(&data->UP3);
    UP4_gen(&data->UP4);
    U_BB_gen(&data->U_BB);
    Circuit_current_A_gen(&data->Circuit_current_A);
    Diff_AP1_gen(&data->Diff_AP1);
    Diff_AP2_gen(&data->Diff_AP2);
    //Diff_AP12_gen(&data->Diff_AP12); //Doesn't work with this function?????
    logics_gen(&data->logics);
    ActiveThreshold_comp_0_gen(&data->ActiveThreshold_comp_0);
    ActiveThreshold_comp_1_gen(&data->ActiveThreshold_comp_1);
    ActiveDiscriminator_comp_0_gen(&data->ActiveDiscriminator_comp_0);
    ActiveDiscriminator_comp_1_gen(&data->ActiveDiscriminator_comp_1);
}

// Temporary variable representing timing events
// To be moved to a dedicated module controlling interrupts from TC1
uint32_t timerTC1;

// This function is to be moved to a dedicated module controlling interrupts from TC1
// We do not need a generic interrupt handler implementation with the callback
void data_ready_interrupt_handler(TC_TIMER_STATUS status, uintptr_t context)
{
    static uint32_t prescaler = 0;
    static uint32_t datagen_prescaler = 0;
    
    if (prescaler == 0) {
        prescaler = 5;
        timerTC1 += 1;
    }
    prescaler -= 1;

    if (datagen_prescaler == 0)
    {
        datagen_prescaler = 1;
        struct agentExampleObj* agent = (struct agentExampleObj*)context;
        get_stream_data(agent->base.exampleAgentAcquisition + agent->base.slotIdx);
        move_ptr(&agent->base, sizeof(struct ExampleAgentAcquisition));
    }
    datagen_prescaler -= 1;
}

struct agentExampleObj *create_example_agent(uint32_t maxFrameBytes, uint16_t port)
{
    uint32_t maxPayloadSize = maxFrameBytes - sizeof(struct Header) - sizeof(crc32);
    struct ProtocolMetaData* proto_control = protocol_create(maxFrameBytes, port);
    struct agentExampleObj *metaData = calloc(1, sizeof(struct agentExampleObj));
    set_base_obj(&metaData->base,
                 &timerTC1,
                 maxPayloadSize,
                 sizeof(struct StreamingHeader),
                 sizeof(struct ExampleAgentAcquisition),
                 proto_control->payload[WRITE_BUF],
                 proto_control);
//    TC1_TimerCallbackRegister(data_ready_interrupt_handler, NULL);
    TC1_TimerCallbackRegister(data_ready_interrupt_handler, (uintptr_t)metaData);
    return metaData;
}

void destroy_example_agent(struct agentExampleObj* agent)
{
    if (agent != NULL)
    {
        protocol_destroy(agent->base.proto_control);
        free(agent);
    }
}

void start_example_agent(struct agentExampleObj* agent)
{
    TC1_TimerStart();
}

void run_example_agent(struct agentExampleObj* agent)
{    
    run_generic_agent(&agent->base);

    if(agent->base.operationStatus == READY) {
        if (agent->base.lastTimerServed != *agent->base.timerEvent) {
            agent->base.lastTimerServed = *agent->base.timerEvent;

 /*           
            get_stream_data(agent->base.exampleAgentAcquisition+agent->slotIdx);
            move_ptr(&agent->base, sizeof(struct ExampleAgentAcquisition));
*/            
            
            size_t rxLen = protocol_read_data(agent->base.proto_control, agent->base.proto_control->header[READ_BUF]);
            if(rxLen > 0) {
                agent->base.proto_control->payload[READ_BUF] = (uint8_t* )((uint8_t *)agent->base.proto_control->header[READ_BUF] + sizeof(struct Header));
                //LED_5G_Toggle();
            }
        }
    }    
}