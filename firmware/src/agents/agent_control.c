#include "agent_control.h"

void set_agent(struct StreamingHeader* payloadControl, struct StreamingAgent agent)
{
    set_2byte(&payloadControl->agent.agentType, (uint16_t)agent.agentType, PROTOCOL_BIG_ENDIAN);
    set_byte(&payloadControl->agent.version, agent.version);
    set_byte(&payloadControl->agent.reserved, agent.reserved);
}

void set_status(struct StreamingHeader* payloadControl, uint16_t status)
{
    set_2byte(&payloadControl->status, status, PROTOCOL_BIG_ENDIAN);
}

void set_index(struct StreamingHeader* payloadControl, uint32_t index)
{
    set_4byte(&payloadControl->index, index, PROTOCOL_BIG_ENDIAN);
}

void set_slots(struct StreamingHeader* payloadControl, uint16_t slots)
{
    set_2byte(&payloadControl->length, slots, PROTOCOL_BIG_ENDIAN);
}

void set_period(struct StreamingHeader* payloadControl, uint32_t period)
{
    set_4byte(&payloadControl->period, period, PROTOCOL_BIG_ENDIAN);
}