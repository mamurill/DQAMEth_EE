#ifndef AGENT_CONTROL_H
#define AGENT_CONTROL_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "byte_swap.h"
#include "compile_options.h"
#include "get_set_helper.h"
#include "tsu.h"
#include "agent_template.h"
#include "agents/agent_template.h"
#include "agents/agent_example1.h"
#include "agents/agent_ptp_log.h"
#include "protocol.h"

enum __attribute__((packed)) DataId
{
    LOGGING = 0x01,
    PM = 0x02,
    CONFIGURATION = 0x03
};

enum __attribute__ ((packed)) AgentType
{
    VOID_AGENT = 256, //HACK added to get the packing to two bytes (as specified by the protocol specs)
    NO_AGENT = 0x00,
    EXAMPLE_AGENT,
    EXAMPLE2_AGENT,
    PTP_AGENT
};

enum __attribute__((__packed__)) StreamingFrameType
{
    STREAMING_LOGGING = 0x01,
    STREAMING_PM = 0x02,
    STREAMING_CONFIG = 0x03
};

#pragma pack(1)
struct __attribute__((packed)) StreamingAgent
{
    enum AgentType agentType;
    uint8_t version;
    uint8_t reserved;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) StreamingHeader
{
    //Agent identification
    struct StreamingAgent agent;
    //Payload type identifier
    uint8_t type;
    /* Controller status*/
    uint16_t status;
    /* Acquisition data index */
    uint32_t index;
    /* Acquisition data slots */
    uint16_t length;
    /* Acquisition time of the first variable */
    uint64_t time;
    //Sampling period of the acquired data
    uint32_t period;
};
#pragma pack()

void set_status(struct StreamingHeader* payloadControl, uint16_t status);

void set_index(struct StreamingHeader* payloadControl, uint32_t index);

void set_agent(struct StreamingHeader* payloadControl, struct StreamingAgent agent);

void set_slots(struct StreamingHeader* payloadControl, uint16_t slots);

void set_period(struct StreamingHeader* payloadControl, uint32_t period);

inline __attribute__((always_inline)) void move_ptr(struct agentBaseObj *metaData, size_t acquisitionSize)
{
    metaData->slotIdx++;
    struct PTP_TIMESTAMP time;
    struct StreamingAgent agent;
    
    agent.agentType = EXAMPLE_AGENT;
    agent.version = 5;
    agent.reserved = 0x00;

    uint64_t timeNS = 0;
    if (metaData->slotIdx == metaData->maxSlots)
    {
        GPIO_PB12_Clear();
        tsu_get_time2(&time, GMAC_REGS->GMAC_TSH, GMAC_REGS->GMAC_TSL, GMAC_REGS->GMAC_TN); //Horrible and Temporary method to obtain timestamp. This method introduces significant delay and Jitter. For optimal performance, the timestamp should be created the instant the event occours
        timeNS = (uint64_t)time_stamp_to_ns(time);
        set_agent(metaData->payloadControl, agent);
        set_slots(metaData->payloadControl, metaData->maxSlots);
        set_period(metaData->payloadControl, 500);
        metaData->payloadControl = protocol_write_data(metaData->proto_control, sizeof(struct StreamingHeader) + metaData->maxSlots * acquisitionSize, timeNS);
        metaData->exampleAgentAcquisition = (struct ExampleAgentAcquisition*)(((uint8_t*)metaData->payloadControl) + sizeof(struct StreamingHeader));
        metaData->slotIdx = 0;
        GPIO_PB12_Set();
    }
}

#endif // AGENT_CONTROL_H
