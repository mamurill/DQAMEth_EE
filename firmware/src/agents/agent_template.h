#ifndef AGENT_TEMPLATE_H
#define AGENT_TEMPLATE_H
#include "protocol_definition.h"

//Agent Acquisition Struct Forward declarations
struct ExampleAgentAcquisition;
struct AgentPtpLogAcquisition;

//Agent Control Struct Forward declarations
struct StreamingHeader;

enum AgentStatus
{
    INIT,
    READY
};

struct agentBaseObj
{
    enum AgentStatus operationStatus;
    uint32_t *timerEvent; 
    uint32_t lastTimerServed;
    uint32_t maxPayloadSize;
    uint32_t slotIdx;
    uint32_t maxSlots;
    struct ProtocolMetaData* proto_control;
    union {
        void* controlPointer;
        struct StreamingHeader* payloadControl;
    };
    union {
        void* acquisitionPointer;
        struct ExampleAgentAcquisition* exampleAgentAcquisition;
        struct AgentPtpLogAcquisition* ptpAgentAcquisition;
    };
};

inline void set_base_obj(struct agentBaseObj *agentBase,
                         uint32_t *timerEvent,
                         uint32_t maxPayloadSize,
                         size_t controlSize,
                         size_t acquisitionSize,
                         void* controlPtr,
                         struct ProtocolMetaData *proto_control)
{
    agentBase->operationStatus = INIT;
    agentBase->maxPayloadSize = maxPayloadSize;
    agentBase->timerEvent = timerEvent;
    if(timerEvent == NULL) {
        agentBase->lastTimerServed = 0;
    }
    else {
        agentBase->lastTimerServed = *agentBase->timerEvent;
    }    
    agentBase->slotIdx = 0;
    agentBase->maxSlots = (maxPayloadSize - controlSize) / acquisitionSize;
    agentBase->proto_control = proto_control;
    agentBase->controlPointer = controlPtr;
    agentBase->acquisitionPointer = controlPtr + controlSize;
}

inline void run_generic_agent(struct agentBaseObj* agent)
{
    switch(agent->operationStatus) {
    case INIT:
        if(protocol_init_socket(agent->proto_control))
        {
            agent->operationStatus = READY;
        }
        break;
    case READY:
        protocol_run(agent->proto_control);
        break;
    default:
        break;
    }
}

#endif // AGENT_TEMPLATE_H