#include "agent_ptp_log.h"

struct AgentPtpLogAcquisition global_ptp_data;
bool ptpDataReady = false;

struct agentPtpLogObj *create_ptp_log_agent(uint32_t maxFrameBytes, uint16_t port)
{
    uint32_t maxPayloadSize = maxFrameBytes - sizeof(struct Header) - sizeof(crc32);
    struct ProtocolMetaData* proto_control = protocol_create(maxFrameBytes, port);
    struct agentPtpLogObj *metaData = calloc(1, sizeof(struct agentPtpLogObj));
    set_base_obj(&metaData->base,
                 NULL,
                 maxPayloadSize,
                 sizeof(struct StreamingHeader),
                 sizeof(struct AgentPtpLogAcquisition),
                 proto_control->payload[WRITE_BUF],
                 proto_control);
    return metaData;
}

void destroy_ptp_log_agent(struct agentPtpLogObj* agent)
{
    if (agent != NULL)
    {
        protocol_destroy(agent->base.proto_control);
        free(agent);
    }
}

void start_ptp_log_agent(struct agentPtpLogObj* agent)
{

}

void run_ptp_log_agent(struct agentPtpLogObj* agent) {

    run_generic_agent(&agent->base);

    if (ptpDataReady == true)
    {
        ptpDataReady = false;
        memcpy(agent->base.ptpAgentAcquisition + agent->base.slotIdx, &global_ptp_data, sizeof(global_ptp_data));
        move_ptr(&agent->base, sizeof(struct AgentPtpLogAcquisition));
    }
}