#include "fifo_buffer.h"

#include <stdlib.h>
#include <string.h>

/**
      @Function
        void increment_indexer(uint16_t* index, const uint16_t* depth); 

      @Summary
        Increments FIFO indexer.

      @Description
        Function implements increment of the indexer of the circular buffer. It manages 
        the overflow of the indexer
      
      @Precondition
        None

      @Parameters
        @param index Pointer to the indexer
        @param depth Number of FIFO slots

      @Returns
        
      @Remarks

     */
inline void increment_indexer(uint16_t* index, const uint16_t* depth);

struct fifo_buffer* fifo_create(uint16_t depth, uint32_t length)
{
    uint16_t i;
    uint8_t **buf;
    struct fifo_buffer* fifo;

    fifo = (struct fifo_buffer*)calloc(1, sizeof(struct fifo_buffer));

    fifo->depth = 0;
    fifo->item_length = 0;
    
    buf = (uint8_t**)(malloc(sizeof(uint8_t*)*depth));
    if (buf == NULL)
        return NULL;
    for (i = 0; i < depth; i++) {
        buf[i] = (uint8_t*)(malloc(sizeof(uint8_t)*length));
        fifo->depth += 1;
        if (buf[i] == NULL)
            return NULL;
    }
    fifo->buffer = buf;
    fifo->item_length = length;
    fifo->read = 0;
    fifo->write = 0;
    fifo->count = 0;
    
    return fifo;
}

void fifo_release(struct fifo_buffer *fifo)
{
    uint16_t i;
    
    if (fifo->buffer != NULL) {
        for (i = 0; i < fifo->depth; i++) {
            if (fifo->buffer[i] == NULL) {
                break;
            }
            else {
                free(fifo->buffer[i]);
                fifo->buffer[i] = NULL;
            }
        }
        free(fifo->buffer);
        fifo->buffer = NULL;
    }

    if (fifo != NULL) {
        free(fifo);
    }
}

void fifo_write(struct fifo_buffer* fifo, const void* data)
{
    memcpy(fifo->buffer[fifo->write], data, fifo->item_length);
    if (fifo->count == fifo->depth)
    {
        increment_indexer(&fifo->write, &fifo->depth);
        increment_indexer(&fifo->read, &fifo->depth);
    }
    else
    {
        increment_indexer(&fifo->write, &fifo->depth);
        fifo->count += 1;
    }
}

void fifo_fast_write(struct fifo_buffer* fifo, void** swap)
{
    uint8_t *tmp = fifo->buffer[fifo->write];
    fifo->buffer[fifo->write] = (uint8_t*)(*swap);
    *swap = (void*)tmp;
    if (fifo->count == fifo->depth)
    {
        increment_indexer(&fifo->write, &fifo->depth);
        increment_indexer(&fifo->read, &fifo->depth);
    }
    else
    {
        increment_indexer(&fifo->write, &fifo->depth);
        fifo->count += 1;
    }
}

bool fifo_read(struct fifo_buffer* fifo, void* data)
{
    if (fifo->count == 0)
    {
        return false;
    }
    else
    {
        memcpy(data, fifo->buffer[fifo->read], fifo->item_length);
        increment_indexer(&fifo->read, &fifo->depth);
        fifo->count -= 1;
        
        return true;
    }

}

void fifo_browse(const struct fifo_buffer* fifo, void* item, uint16_t index)
{
    const uint8_t *data = fifo_peek(fifo, index);
    memcpy(item, data, fifo->item_length);
}

const void* fifo_peek(const struct fifo_buffer* fifo, uint16_t index)
{
    uint16_t readIndex;

    if (index >= fifo->depth)
    {
        index = index % fifo->depth;
    }
    readIndex = fifo->read + index;
    if (readIndex >= fifo->depth)
    {
        readIndex -= fifo->depth; 
    }

    return (const uint8_t*)fifo->buffer[readIndex];
}

uint16_t fifo_count(const struct fifo_buffer* fifo)
{    
    return fifo->count;
}

void fifo_clear(struct fifo_buffer* fifo)
{
    fifo->read = 0;
    fifo->write = 0;
    fifo->count = 0;
}

void increment_indexer(uint16_t* index, const uint16_t* depth)
{
    *index += 1;
    if (*index == *depth)
    {
        *index = 0;
    }
}