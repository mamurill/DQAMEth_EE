#include "custom_gmac_lib_samE5x.h"

/****************************************************************************
 * GMAC Interrupt Service Routines(ISR)
 *****************************************************************************/
//GMAC interrupt handler for Priority Queue 0
void GMAC_InterruptHandler(void)
{
    uint32_t  all_events = GMAC_REGS->GMAC_ISR;
    GMAC_EVENTS  currEthEvents = (GMAC_EVENTS)all_events;
    GMAC_REGS->GMAC_IDR = currEthEvents;
    if(currEthEvents & GMAC_EV_RX_ALL)
    {
        drvGmacQueEvents |= GMAC_QUE0_MASK;
    }
    __DMB();
	DRV_GMAC_Tasks_ISR((SYS_MODULE_OBJ)0, (uint32_t)all_events);
}
