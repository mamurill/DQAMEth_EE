#ifndef DQAME_V2_0_BSP_H    /* Guard against multiple inclusion */
#define DQAME_V2_0_BSP_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "peripheral/port/plib_port.h"

/*** Macros for LED_5G pin ***/
//#define LED_5G_Set()              GPIO_PA00_Set()           
//#define LED_5G_Clear()            GPIO_PA00_Clear()         
//#define LED_5G_Toggle()           GPIO_PA00_Toggle()        
//#define LED_5G_OutputEnable()     GPIO_PA00_OutputEnable()  
//#define LED_5G_InputEnable()      GPIO_PA00_InputEnable()   
//#define LED_5G_Get()              GPIO_PA00_Get()           
//#define LED_5G_PIN                GPIO_PA00_PIN             
                                  
/*** Macros for LED_MCU_Status pin ***/   
#define LED_MCU_Status_Set()              GPIO_PA01_Set()           
#define LED_MCU_Status_Clear()            GPIO_PA01_Clear()         
#define LED_MCU_Status_Toggle()           GPIO_PA01_Toggle()        
#define LED_MCU_Status_OutputEnable()     GPIO_PA01_OutputEnable()  
#define LED_MCU_Status_InputEnable()      GPIO_PA01_InputEnable()   
#define LED_MCU_Status_Get()              GPIO_PA01_Get()           
#define LED_MCU_Status_PIN                GPIO_PA01_PIN    

/*** Macros for LEMO pin ***/   
#define LEMO_Set()                        GPIO_PC00_Set()           
#define LEMO_Clear()                      GPIO_PC00_Clear()         
#define LEMO_Toggle()                     GPIO_PC00_Toggle()        
#define LEMO_OutputEnable()               GPIO_PC00_OutputEnable()  
#define LEMO_InputEnable()                GPIO_PC00_InputEnable()   
#define LEMO_Get()                        GPIO_PC00_Get()           
#define LEMO_PIN                          GPIO_PC00_PIN 

/*** Macros for LED_MCU_1 pin ***/   
#define LED_MCU_1_Set()              GPIO_PC07_Set()           
#define LED_MCU_1_Clear()            GPIO_PC07_Clear()         
#define LED_MCU_1_Toggle()           GPIO_PC07_Toggle()        
#define LED_MCU_1_OutputEnable()     GPIO_PC07_OutputEnable()  
#define LED_MCU_1_InputEnable()      GPIO_PC07_InputEnable()   
#define LED_MCU_1_Get()              GPIO_PC07_Get()           
#define LED_MCU_1_PIN                GPIO_PC07_PIN 

/*** Macros for LED_MCU_2 pin ***/   
#define LED_MCU_2_Set()              GPIO_PC24_Set()           
#define LED_MCU_2_Clear()            GPIO_PC24_Clear()         
#define LED_MCU_2_Toggle()           GPIO_PC24_Toggle()        
#define LED_MCU_2_OutputEnable()     GPIO_PC24_OutputEnable()  
#define LED_MCU_2_InputEnable()      GPIO_PC24_InputEnable()   
#define LED_MCU_2_Get()              GPIO_PC24_Get()           
#define LED_MCU_2_PIN                GPIO_PC24_PIN 

/*** Macros for LED_MCU_3 pin ***/   
#define LED_MCU_3_Set()              GPIO_PC13_Set()           
#define LED_MCU_3_Clear()            GPIO_PC13_Clear()         
#define LED_MCU_3_Toggle()           GPIO_PC13_Toggle()        
#define LED_MCU_3_OutputEnable()     GPIO_PC13_OutputEnable()  
#define LED_MCU_3_InputEnable()      GPIO_PC13_InputEnable()   
#define LED_MCU_3_Get()              GPIO_PC13_Get()           
#define LED_MCU_3_PIN                GPIO_PC13_PIN 

/*** Macros for LED_MCU_4 pin ***/   
#define LED_MCU_4_Set()              GPIO_PC19_Set()           
#define LED_MCU_4_Clear()            GPIO_PC19_Clear()         
#define LED_MCU_4_Toggle()           GPIO_PC19_Toggle()        
#define LED_MCU_4_OutputEnable()     GPIO_PC19_OutputEnable()  
#define LED_MCU_4_InputEnable()      GPIO_PC19_InputEnable()   
#define LED_MCU_4_Get()              GPIO_PC19_Get()           
#define LED_MCU_4_PIN                GPIO_PC19_PIN 
                                                              
    /*** Macros for LED_1 pin ***/    
#define LED_1_Set()               GPIO_PA27_Set()           
#define LED_1_Clear()             GPIO_PA27_Clear()         
#define LED_1_Toggle()            GPIO_PA27_Toggle()        
#define LED_1_OutputEnable()      GPIO_PA27_OutputEnable()  
#define LED_1_InputEnable()       GPIO_PA27_InputEnable()   
#define LED_1_Get()               GPIO_PA27_Get()           
#define LED_1_PIN                 GPIO_PA27_PIN             
                                 
/*** Macros for PHY RESET pin ***/    
#define PHY_Set()               GPIO_PC10_Set()           
#define PHY_Clear()             GPIO_PC10_Clear()         
#define PHY_Toggle()            GPIO_PC10_Toggle()        
#define PHY_OutputEnable()      GPIO_PC10_OutputEnable()  
#define PHY_InputEnable()       GPIO_PC10_InputEnable()   
#define PHY_Get()               GPIO_PC10_Get()           
#define PHY_PIN                 GPIO_PC10_PIN

#ifdef	__cplusplus
}
#endif

#endif /* DQAME_V2_0_BSP_H */

