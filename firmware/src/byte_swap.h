#ifndef BYTE_SWAP_H
#define BYTE_SWAP_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "configuration.h"
#include "definitions.h"
#include "DQAME_V2_0_bsp.h"

//Functions swaps byte ordering. This allows a little endian data type to become big endian, or vice versa
#define bswap16(data, dest) (*dest = __builtin_bswap16(data))
#define bswap32(data, dest) (*dest = __builtin_bswap32(data))
//#define bswap64(data, dest) (*dest = __builtin_bswap64(data)) //This function is EVIL. It crashes the MCU, as the compiler has a hard time operating on 64 bit unalligned data.

//Alternative to __builtin_bswap64. MEGA HACK to allow for unalligned 64 bit swap
inline __attribute__((always_inline)) void bswap64(uint64_t data, uint64_t* dest)
{
    uint64_t tmp = __builtin_bswap64(data);
    memcpy(dest, &tmp, 8);
}

#endif // BITSWAP_H
