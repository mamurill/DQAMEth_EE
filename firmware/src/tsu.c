#include "tsu.h"

#define PLL_FRQ 100000000
#define PLL_PERIOD_NS 10 
#define SUBNS_RES 0.0000152587890625
#define SUB10NS_RES 0.000152587890625

/*******************************************************************
 * NAME :            tsu_get_time(struct PTP_TIMESTAMP *time, __IO uint32_t TSH, __IO uint32_t TSL, __IO uint32_t TN)
 *
 * DESCRIPTION :     Gets the current time from an 80 bit wide Time stamp unit register and saves it to a location in memory
 *
 * INPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *time               Location in memory to store the timestamp
 *           __IO uint32_t               TSH                 Register containing the 16 HIGH bits   
 *           __IO uint32_t               TSL                 Register containing the 32 LOW bits
 *           __IO uint32_t               TN                  Register containing the 32 LOWEST bits representing NANO seconds
 * OUTPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *time
 */
void tsu_get_time(struct PTP_TIMESTAMP *time, __IO uint32_t TSH, __IO uint32_t TSL, __IO uint32_t TN)
{
    time->secondsField = 0;

    time->secondsField = ((uint64_t) TSH) << 32;
    time->secondsField |= TSL;

    time ->nanosecondsField = TN;
}

void tsu_get_time2(struct PTP_TIMESTAMP *time, __IO uint32_t TSH, __IO uint32_t TSL, __IO uint32_t TN)
{
    struct PTP_TIMESTAMP tmp;
    struct PTP_TIMESTAMP oddStmp;
    
    tmp.subtract = false;
    tmp.nanosecondsField = 0;
    tmp.secondsField = 0;
    oddStmp.subtract = false;
    oddStmp.nanosecondsField = 0;
    oddStmp.secondsField = 0;
    
    tsu_get_time(&tmp,TSH,TSL,TN);
    
    tmp.nanosecondsField = tmp.nanosecondsField >> PRES_EXTENSION;
    
    if(PRES_EXTENSION == 1)
    {
        oddStmp.nanosecondsField = (tmp.secondsField & 0x01)*500000000;
    }
    else if(PRES_EXTENSION == 2)
    {
        oddStmp.nanosecondsField = ((tmp.secondsField >> 1) & 0x01)*500000000 + ((tmp.secondsField >> 0) & 0x01)*250000000 ;
    }
    else if(PRES_EXTENSION == 3)
    {
        oddStmp.nanosecondsField = ((tmp.secondsField >> 2) & 0x01)*500000000 + ((tmp.secondsField >> 1) & 0x01)*250000000 + ((tmp.secondsField >> 0) & 0x01)*125000000;
    }
    else if(PRES_EXTENSION == 4)
    {
        oddStmp.nanosecondsField = ((tmp.secondsField >> 3) & 0x01)*500000000 + ((tmp.secondsField >> 2) & 0x01)*250000000 + ((tmp.secondsField >> 1) & 0x01)*125000000 + ((tmp.secondsField >> 0) & 0x01)*62500000;
    }
    else
    {
        oddStmp.nanosecondsField = 0;
    }
    
    tmp.secondsField = tmp.secondsField >> PRES_EXTENSION;
    time_stamp_add(&tmp,&oddStmp,time);
}


/*******************************************************************
 * NAME :            tsu_set_time(struct PTP_TIMESTAMP *time)
 *
 * DESCRIPTION :     Sets the time in the Time stamp units timer register
 *
 * INPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *time               Location in memory where the timestamp to be written is stored
 */
void tsu_set_time(struct PTP_TIMESTAMP *time)
{
    GMAC_REGS->GMAC_TSH = (uint32_t) (time->secondsField >> 32);
    GMAC_REGS->GMAC_TSL = (uint32_t) (time->secondsField & 0x00000000FFFFFFFF);
    GMAC_REGS->GMAC_TN = time->nanosecondsField & ~(0xC0000000);
}

void tsu_set_time2(struct PTP_TIMESTAMP *time)
{
    uint8_t i = 0;
    struct PTP_TIMESTAMP tmp = *time;
    for(i = 0; i<PRES_EXTENSION;i++)
    {
        time_stamp_add(&tmp,&tmp,&tmp);   
    }
    tsu_set_time(&tmp);
}

/*******************************************************************
 * NAME :            tsu_comp_set_time(struct PTP_TIMESTAMP *time)
 *
 * DESCRIPTION :     Sets the time in the Time stamp units comparison timer register
 *                   Only the upper 21 bits of the nanosecond register is set
 *
 * INPUTS :
 *       PARAMETERS:
 *           struct PTP_TIMESTAMP               *time               Location in memory where the timestamp to be written is stored
 */
void tsu_comp_set_time(struct PTP_TIMESTAMP *time)
{
    GMAC_REGS->GMAC_SCH = (uint32_t) (time->secondsField >> 32);
    GMAC_REGS->GMAC_SCL = (uint32_t) (time->secondsField & 0x00000000FFFFFFFF);
    GMAC_REGS->GMAC_NSC = (time->nanosecondsField >> 8) & 0x00000000003FFFFF;
}

void tsu_comp_set_time2(struct PTP_TIMESTAMP *time)
{
    uint8_t i = 0;
    struct PTP_TIMESTAMP tmp = *time;
    for(i = 0; i<PRES_EXTENSION;i++)
    {
        time_stamp_add(&tmp,&tmp,&tmp);   
    }
    tsu_comp_set_time(&tmp);
}

/*******************************************************************
 * NAME :            TSU_adjust(uint32_t nanoseconds, bool subtract)
 *
 * DESCRIPTION :     Adjust the TSU timer register by up to +/- 1 second (1000000000 Nano seconds)
 *
 * INPUTS :
 *       PARAMETERS:
 *           uint32_t                    nanoseconds         number of nanoseconds to adjust by
 *           bool                        subtract            True to subtract, False to add
 */
void TSU_adjust(uint32_t nanoseconds, bool subtract)
{
    GMAC_REGS->GMAC_TA = (subtract << 31) | (nanoseconds & 0x3FFFFFFF);
}

double get_rate()
{
    uint8_t nanoSec = (uint8_t)(GMAC_REGS->GMAC_TI & (0xFF));
    uint16_t subNanoSec = (uint16_t)(GMAC_REGS->GMAC_TISUBN & (0xFFFF));
    
    double ratio = (nanoSec+subNanoSec*SUBNS_RES)/PLL_PERIOD_NS;
    ratio = ratio/(1 << PRES_EXTENSION);
    return ratio;
}

double ratio;
uint8_t nanoSec;
uint32_t subNanoSecTemp;
uint16_t subNanoSec;
double set_rate(double target)
{
    ratio = target*(1 << PRES_EXTENSION);
    nanoSec = floor(ratio*PLL_PERIOD_NS);
    subNanoSecTemp = (uint32_t)round((((ratio*PLL_PERIOD_NS)-nanoSec)/(SUBNS_RES))); //Watch out for overflow here
    subNanoSec = (uint16_t) subNanoSecTemp; 
    if(subNanoSecTemp>65535)
    {
        subNanoSec = 65535;
    }
    GMAC_REGS->GMAC_TI = nanoSec;
    GMAC_REGS->GMAC_TISUBN = subNanoSec;
    return get_rate();
}