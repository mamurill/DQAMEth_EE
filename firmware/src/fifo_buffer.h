#ifndef FIFO_BUFFER_H
#define FIFO_BUFFER_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

struct fifo_buffer
{
    /* FIFO buffer */
    uint8_t **buffer;
    /* FIFO read pointer */
    uint16_t read;
    /* FIFO write pointer */
    uint16_t write;
    /* FIFO number of slots, size of the FIFO */
    uint16_t depth;
    /* FIFO slot length */
    uint32_t item_length;
    /* FIFO number of elements */
    uint16_t count;
};

/**
     @Function
    struct fifo_buffer* fifo_create(uint16_t depth, uint32_t length) 

    @Summary
    Creates FIFO buffer.

    @Description
    Function initialises fifo data structure used in fifo operations and
    allocates memory required for the two dimensional fifo buffer.
    
    @Precondition
    None

    @Parameters
    @param fifo Pointer to the FIFO structure
    @param depth Number of slots in the FIFO, expressed as 2^depth.
    @param length Length of the slot in the FIFO

    @Returns
    A valid pointer to the creted fifo structure. In case of failure,
    it returns NULL.
    @Remarks

*/
struct fifo_buffer* fifo_create(uint16_t depth, uint32_t length);

/**
     @Function
    void fifo_release(struct fifo_buffer* fifo) 

    @Summary
    Releases FIFO buffer.

    @Description
    Function releases fifo buffer and resets the fifo data structure.
    
    @Precondition
    None

    @Parameters
    @param fifo Pointer to the FIFO structure

    @Returns
    
    @Remarks

*/
void fifo_release(struct fifo_buffer* fifo);

/**
     @Function
    void fifo_write(struct fifo_buffer* fifo, const void* data) 

    @Summary
    Writes data to the FIFO buffer.

    @Description
    Function writes data to the selected FIFO buffer. FIFO pointers are
    managed by the function. Function advances the write pointer.
        
    @Precondition
    FIFO must be initialised.

    @Parameters
    @param fifo Pointer to the FIFO buffer.
    @param data Pointer to the source of data

    @Returns
    
    @Remarks

*/
void fifo_write(struct fifo_buffer* fifo, const void* data);

/**
     @Function
    void fifo_fast_write(struct fifo_buffer* fifo, const void* data) 

    @Summary
    Writes data to the FIFO buffer.

    @Description
    Function writes data to the selected FIFO buffer. It swaps memory pointers,
    hence the user must ensure they are compatible. FIFO pointers are
    managed by the function. Function advances the write pointer.
        
    @Precondition
    FIFO must be initialised. The swap parameter must be initialized and
    swapable.

    @Parameters
    @param fifo Pointer to the FIFO buffer.
    @param swap Address of the pointer of the source of data

    @Returns
    
    @Remarks

*/
void fifo_fast_write(struct fifo_buffer* fifo, void** swap);

/**
     @Function
    bool fifo_read(struct fifo_buffer* fifo, void* data) 

    @Summary
    Reads data from the FIFO buffer.

    @Description
    Function reads data from the selected FIFO buffer if available. The 
    availability is assessed by comparing write and read pointers. FIFO 
    pointers are managed by the function. Function advances the read
    pointer. 
        
    @Precondition
    FIFO must be initialised.

    @Parameters
    @param fifo Pointer to the FIFO buffer.
    @param data Pointer to the destination where data from FIFO is copied.

    @Returns
    Returns true provided data was successfully copied.
    
    @Remarks

*/
bool fifo_read(struct fifo_buffer* fifo, void* data);

/**
     @Function
    void fifo_browse(const struct fifo_buffer* fifo, void* item, uint16_t index) 

    @Summary
    Enables to copy a FIFO element. It does not advance FIFO pointers.

    @Description
    Function enables to browse data stored in the FIFO buffer. Addressing is
    controlled by signed index parameter. The function does not take data
    from the buffer, hence the pointers are not changed.
        
    @Precondition
    FIFO must be initialised.

    @Parameters
    @param fifo Pointer to a FIFO buffer.
    @param item Pointer to a destination where data from FIFO is copied.
    @param index Signed index to move read pointer.

    @Returns
    
    @Remarks

*/
void fifo_browse(const struct fifo_buffer* fifo, void* item, uint16_t index);

/**
     @Function
    const void* fifo_peek(const struct fifo_buffer* fifo, uint16_t index) 

    @Summary
    Enables to peek a FIFO element.

    @Description
    Function provides a direct access to data stored in the FIFO buffer. As the
    funtion exposes the pointer, user shall copy it as soon as possible. This feature
    must not be used to proces data stored on the fifo.
        
    @Precondition
    FIFO must be initialised.

    @Parameters
    @param fifo Pointer to a FIFO buffer.
    @param index Signed index to move read pointer.

    @Returns
    Direct pointer to a FIFO buffer

    @Remarks

*/
const void* fifo_peek(const struct fifo_buffer* fifo, uint16_t index);

/**
     @Function
    uint16_t fifo_count(const fifo_type* fifo) 

    @Summary
    Returns the number of FIFO items.

    @Description
    Function returns number of items stored in the buffer.
        
    @Precondition
    FIFO must be initialised.

    @Parameters
    @param fifo Pointer to a FIFO buffer.

    @Returns
    Number of items;
    
    @Remarks

*/
uint16_t fifo_count(const struct fifo_buffer* fifo);

/**
     @Function
    void fifo_clear(struct fifo_buffer* fifo)

    @Summary
    Cleares FIFO buffer.

    @Description
    Function sets data indexers and data count to zero. The memory content is unaffected.
    
    @Precondition
    None

    @Parameters
    @param fifo Pointer to the FIFO structure

    @Returns
    
    @Remarks

*/
void fifo_clear(struct fifo_buffer* fifo);

#endif