/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */


/* This section lists the other files that are included in this file.
 */

#include "ethernet_dev.h"

enum EthernetStatus
{
  ETH_STATE_TCP_IP_INIT,
  ETH_STATE_UDP_INIT,
  ETH_STATE_WAIT_INIT,
  ETH_STATE_BIND,
  ETH_STATE_WAIT_BIND,
  ETH_STATE_READY
};

/** ConnectionData

  @Summary
    Connection data type    
    
  @Description 
    Data type specifies all the data required to initialize and maintain a UDP socket
        
*/
struct ConnectionData
{
  enum EthernetStatus ethStatus;
  IP_MULTI_ADDRESS hostIp;
  IP_MULTI_ADDRESS localIp;
  UDP_PORT port;
  enum ConnectionTypes connectionType;
  bool enableSignal;
  TCPIP_UDP_SIGNAL_TYPE interruptSignals;
  TCPIP_UDP_SIGNAL_FUNCTION event;
  void* signalContext;
  UDP_SOCKET socket;
  bool connectionParamsCorrect;
  SYS_TIME_HANDLE timer;
};

//Forward Declaration Initalization functions
bool check_TCPIP_STACK_init(struct ConnectionData* connData);
bool configure_udp_socket(struct ConnectionData* connData);
bool check_udp_connection(struct ConnectionData* connData);
bool bind_socket_port(struct ConnectionData* connData);
bool check_bind(struct ConnectionData* connData);

//Stolen from: https://stackoverflow.com/questions/15653695/how-to-convert-ip-address-in-char-to-uint32-t-in-c
static bool str_ip_to_int32(char *ipStr, uint32_t *pIpAddr)
{
  unsigned int byte3 = 0;
  unsigned int byte2 = 0;
  unsigned int byte1 = 0;
  unsigned int byte0 = 0;
  char dummyString[2];

  /* The dummy string with specifier %1s searches for a non-whitespace char
    * after the last number. If it is found, the result of sscanf will be 5
    * instead of 4, indicating an erroneous format of the ip-address.
    */
  if (sscanf(ipStr, "%u.%u.%u.%u%1s",
             &byte3, &byte2, &byte1, &byte0, dummyString) == 4)
  {
    if ((byte3 < 256) && (byte2 < 256) && (byte1 < 256) && (byte0 < 256))
    {
      *pIpAddr = (byte0 << 24) + (byte1 << 16) + (byte2 << 8) + byte3;

      return true;
    }
  }

  return false;
}

static bool set_ip_field(struct ConnectionData *connData, char *ipStr)
{

  return str_ip_to_int32(ipStr, &connData->hostIp.v4Add.Val);
}

static bool set_event_field(struct ConnectionData *connData, TCPIP_UDP_SIGNAL_HANDLE event)
{
  if (event != NULL)
  {
    connData->event = event;
    return true;
  }
  else
  {
    return false;
  }
}

bool check_TCPIP_STACK_init(struct ConnectionData* connData)
{
  SYS_STATUS tcpipStat;
  // sysObj.tcpip is defined in "initialization.c" it stores object handles for the mplab harmony systems
  tcpipStat = TCPIP_STACK_Status(sysObj.tcpip);
  if (tcpipStat == SYS_STATUS_READY)
  {
    return true;
  }
  else
  {
    return false;
  }
}


bool configure_udp_socket(struct ConnectionData* connData)
{
  if (connData->connectionType == CLIENT)
  {
    connData->socket = TCPIP_UDP_ClientOpen(IP_ADDRESS_TYPE_IPV4, connData->port, &connData->hostIp);
  }
  else
  {
    connData->socket = TCPIP_UDP_ServerOpen(IP_ADDRESS_TYPE_IPV4, connData->port, NULL);
  }
  if(connData->enableSignal)
  {
    TCPIP_UDP_SignalHandlerRegister(connData->socket,
                                    connData->interruptSignals,
                                    connData->event,
                                    connData->signalContext);
  }
  return true;
}


bool check_udp_connection(struct ConnectionData* connData)
{
  if (TCPIP_UDP_IsConnected(connData->socket))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool bind_socket_port(struct ConnectionData* connData)
{
  bool success = TCPIP_UDP_Bind(connData->socket, IP_ADDRESS_TYPE_IPV4, connData->port, 0);
  connData->timer = SYS_TIME_HANDLE_INVALID;
  if (success)
  {
    return true;  
  }
  else
  {
    return false;
  }
}

bool check_bind(struct ConnectionData* connData)
{
  // It is not possible to retrive this information from the socket.
  // The bind process takes some time and hence the arbitrary timeout.
  if (connData->timer == SYS_TIME_HANDLE_INVALID)
  {
    if (SYS_TIME_DelayMS(5000, &connData->timer) != SYS_TIME_SUCCESS)
    {
      connData->timer = SYS_TIME_HANDLE_INVALID;
      return false;
    }
  }

  bool success = SYS_TIME_DelayIsComplete(connData->timer);
  if (success)
  {
      return true;
  }
  else
  {
    return false;
  }
}

struct ConnectionData *ConnectionData_create()
{
  struct ConnectionData *connData = calloc(1, sizeof(struct ConnectionData));
  connData->connectionParamsCorrect = false;
  return connData;
}

void ConnectionData_destroy(struct ConnectionData *connData)
{
  if (connData != NULL)
  {
    free(connData);
    connData = NULL;
  }
}

bool ConnectionData_setup(struct ConnectionData *connData, UDP_PORT port, enum ConnectionTypes connectionType,bool enableSignal, TCPIP_UDP_SIGNAL_TYPE interruptSignals, TCPIP_UDP_SIGNAL_FUNCTION event, void* context, char *ipStr)
{
  bool errCode = true;
  connData->port = port;
  connData->connectionType = connectionType;
  errCode &= set_ip_field(connData, ipStr);
  connData->enableSignal = enableSignal;
  connData->interruptSignals = interruptSignals;
  connData->event = event;
  connData->signalContext = context;
  errCode &= set_event_field(connData, event);
  connData->socket = INVALID_UDP_SOCKET;
  connData->connectionParamsCorrect = errCode;
  connData->ethStatus = ETH_STATE_TCP_IP_INIT;
  return errCode;
}

UDP_SOCKET get_socket(struct ConnectionData *connData)
{
  return connData->socket;
}

bool eth_run_init(struct ConnectionData *connData)
{
  bool success = false;
  switch (connData->ethStatus)
  {
    case ETH_STATE_TCP_IP_INIT:
      if (check_TCPIP_STACK_init(connData))
      {
        connData->ethStatus = ETH_STATE_UDP_INIT;
      }
      break;
    case ETH_STATE_UDP_INIT:
      if (configure_udp_socket(connData))
      {
        connData->ethStatus = ETH_STATE_WAIT_INIT;
      }
      break;
    case ETH_STATE_WAIT_INIT:
      if (check_udp_connection(connData))
      {
        connData->ethStatus = ETH_STATE_BIND;
      }
      break;
    case ETH_STATE_BIND:
      if (bind_socket_port(connData))
      {
        connData->ethStatus = ETH_STATE_WAIT_BIND;
      }
      break;
    case ETH_STATE_WAIT_BIND:
      if (check_bind(connData))
      {
        connData->ethStatus = ETH_STATE_READY;
        success = true;
      }
      break;
    case ETH_STATE_READY:
      success = true;
      break;
    default:
      break;
  }

  return success;
}

uint16_t transmit_data(struct ConnectionData *connData, const uint8_t *cData, uint16_t wDataLen)
{
  uint16_t availableLen;
  availableLen = TCPIP_UDP_PutIsReady(connData->socket);
  if (availableLen >= wDataLen)
  {
    availableLen = TCPIP_UDP_ArrayPut(connData->socket, cData, wDataLen);
    availableLen = TCPIP_UDP_Flush(connData->socket);
    return availableLen;
  }
  return 0;
}

uint16_t read_data(struct ConnectionData *connData, uint8_t* dest, uint16_t size)
{
  uint16_t len;
  TCPIP_UDP_GetIsReady(connData->socket);
  len = TCPIP_UDP_ArrayGet(connData->socket, dest, size);
  TCPIP_UDP_Discard(connData->socket);
  return len;
}

uint16_t discard_data(struct ConnectionData *connData)
{
  return TCPIP_UDP_Discard(connData->socket);
}


/* *****************************************************************************
 End of File
 */
