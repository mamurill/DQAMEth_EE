#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "byte_swap.h"
#include "ethernet_dev.h"
#include "compile_options.h"
#include "crc32.h"
#include "DQAME_V2_0_bsp.h"
#include "fifo_buffer.h"
#include "get_set_helper.h"
#include "protocol_definition.h"

#define FRAMEBUFFER_SIZE 20 //Limit 98
#define READFRAMEBUFFER_SIZE 4
#define PROTOCOL_VERSION 5

typedef uint32_t crc32;

struct __attribute__((packed)) float_s7e23m
{
    uint32_t exponent:24;
    uint32_t mantissa:7;
    uint32_t sign:1;
};

struct __attribute__((packed)) float_s15e16m
{
    uint32_t exponent:16;
    uint32_t mantissa:15;
    uint32_t sign:1;
};

enum __attribute__((packed)) FrameControl
{
    FRAME_NORMAL = 0x00,
    FRAME_NACK = 0x01,
    FRAME_RETRANSMIT = 0x02,
    FRAME_ACK = 0x03
};

enum __attribute__((packed)) TrafficType
{
    TRAFFIC_STREAM = 0x01,
    TRAFFIC_BUFFERED_STREAM = 0x02,
    TRAFFIC_CONTROL = 0x04
};

struct __attribute__((__packed__)) FrameType
{
    uint8_t trafficType : 6, frameControl : 2; //Can not use enum in definition due to bit field limitations
};

struct __attribute__((packed)) ProtocolTime
{
    /* A second resolution time component represented in Unix time */
    uint32_t timeSecs;
    /* A nanosecond resolution extension time component */
    uint32_t timeNanosecs;
};

enum ActiveFrameBufferIdx
{
    WRITE_BUF,
    READ_BUF,
    TX_BUF,
    RX_BUF,
    
    ACTIVE_FRAME_BUFFER_IDX_COUNT
};

//Reason for #pragma https://stackoverflow.com/questions/24887459/c-c-struct-packing-not-working
#pragma pack(1)
struct __attribute__((packed)) Header
{
    //Version of the protocol specification used on the transmitting device as (x.y * 10)
    uint8_t version;
    //Frame type identifier
    uint8_t type;
    /* Length of the the frame*/
    uint16_t length;
    /* Frame Unique ID */
    uint32_t uid;
    /* Frame counter */
    uint32_t index;
    /* Acquisition timestamp */
    uint64_t time;
};
#pragma pack()

 struct ProtocolMetaData
{
    struct ConnectionData *conn_data;
    size_t FrameSize;
    uint32_t frameCounter;
    uint32_t rxQueueLen;
    struct fifo_buffer *frameBuffer;
    struct fifo_buffer* readFrameBuffer;
    uint32_t uid;
    struct Header *header[ACTIVE_FRAME_BUFFER_IDX_COUNT];
    uint8_t* payload[ACTIVE_FRAME_BUFFER_IDX_COUNT];
};

bool protocol_init_socket(struct ProtocolMetaData* proto_data);
struct StreamingHeader* protocol_write_data(struct ProtocolMetaData* context, uint16_t payloadLength, uint64_t timestamp);
void protocol_dispatch_queued(struct ProtocolMetaData* protoData);
struct ProtocolMetaData* protocol_create(size_t maxFrameBytes, uint16_t port);
void protocol_destroy(struct ProtocolMetaData *protocolData);
void protocol_run(struct ProtocolMetaData* protoData);
uint32_t protocol_read_data(struct ProtocolMetaData* proto_data, struct Header* dest);
#endif // PROTOCOL_H
