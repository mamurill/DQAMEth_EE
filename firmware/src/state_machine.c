#include "state_machine.h"


//Implementation taken from: https://stackoverflow.com/questions/1647631/c-state-machine-design/1647679#1647679
bool state_machine_execute(struct StateMachineMetaData *data)
{
    EventObj event;
    if (data->current_state != ST_TERM)
    {
        if (!fifo_read(data->eventQueue, &event))
        {
            event = EV_NONE;
        }
        for (uint32_t i = 0; i < data->transitionCount; i++)
        {
            if ((data->current_state == (*(data->trans))[i].transConditions.st) || (ST_ANY == (*(data->trans))[i].transConditions.st))
            {
                if ((event == (*(data->trans))[i].transConditions.ev) || (EV_ANY == (*(data->trans))[i].transConditions.ev))
                {
                    data->previous_state = data->current_state;
                    data->current_state = data->execute(data, i);
                    return false;
                }
            }
        }
        return false;
    }
    return true;
}

void StateMachine_create(struct StateMachineMetaData *stateMachine, uint32_t (*execute)(struct StateMachineMetaData *, uint32_t), uint32_t init_state, uint32_t transCount, void *transitionObj, uint32_t eventBufferSize)
{
    stateMachine->execute = execute;
    stateMachine->current_state = init_state;
    stateMachine->previous_state = ST_NULL;
    stateMachine->transitionCount = transCount;
    stateMachine->trans = (struct TransitionObj(*)[])transitionObj;
    stateMachine->eventQueue = fifo_create(eventBufferSize, sizeof(EventObj));
}

void StateMachine_destroy(struct StateMachineMetaData *stateMachine)
{
    fifo_release(stateMachine->eventQueue);
    stateMachine->eventQueue = NULL;
    if (stateMachine != NULL)
    {
        free(stateMachine);
    }
}

bool Queue_event(struct StateMachineMetaData *data, EventObj* event)
{
   fifo_write(data->eventQueue, event);
   return true;
}

void Queue_clear(struct StateMachineMetaData *data)
{
    fifo_clear(data->eventQueue);
}

uint32_t get_state(struct StateMachineMetaData *data)
{
    return data->current_state;
}