/* 
 * File:   custom_definitions.h
 * Author: magnu
 *
 * Created on 14. januar 2021, 11:08
 */

#ifndef CUSTOM_DEFINITIONS_H
#define	CUSTOM_DEFINITIONS_H

#ifdef	__cplusplus
extern "C" {
#endif


#define BILLION 1000000000

#ifdef	__cplusplus
}
#endif

#endif	/* CUSTOM_DEFINITIONS_H */

