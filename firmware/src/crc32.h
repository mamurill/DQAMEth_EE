#ifndef CRC32_H
#define CRC32_H

#include <stdint.h>
    /**
      @Function
        uint32_t crc32b(const uint8_t *frame, uint16_t length) 

      @Summary
        Returns CRC for a frame.

      @Description
        Function calculates CRC value for a frame of defined type.
        This is the basic CRC-32 calculation with some optimization but no
        table lookup. The byte reversal is avoided by shifting the crc reg
        right instead of left and by using a reversed 32-bit word to represent
        the polynomial.

      @Precondition
        None

      @Parameters
        @param frame Pointer to a data frame.
        @param length Length of the frame.

      @Returns
        CRC value

      @Remarks

     */
    uint32_t crc32b(const uint8_t *frame, uint16_t length);

#endif // CRC32_H
