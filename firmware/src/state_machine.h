#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <stdint.h>
#include "fifo_buffer.h"

#define ST_ERROR -1
#define ST_NULL 0
#define ST_TERM 1
#define ST_ANY 2
#define EV_NONE -1
#define EV_ANY 0


struct TransitionObj{
    uint32_t st;
    uint32_t ev;
};

struct TransitionObjProto{
    struct TransitionObj transConditions;
    uint32_t (*fn)(void);
};

typedef uint32_t EventObj;

struct StateMachineMetaData{
    uint32_t (*execute)(struct StateMachineMetaData*, uint32_t);
    uint32_t current_state;
    uint32_t previous_state;
    uint32_t transitionCount;
    struct fifo_buffer* eventQueue;
    struct TransitionObjProto (*trans)[];
};

bool state_machine_execute(struct StateMachineMetaData* data);
void StateMachine_create(struct StateMachineMetaData* stateMachine, uint32_t (*execute)(struct StateMachineMetaData*, uint32_t), uint32_t init_state, uint32_t transCount, void* transitionObj, uint32_t eventBufferSize);
void StateMachine_destroy(struct StateMachineMetaData* stateMachine);
bool Queue_event(struct StateMachineMetaData* data, EventObj* event);
void Queue_clear(struct StateMachineMetaData* data);
uint32_t get_state(struct StateMachineMetaData *data);
#endif // STATE_MACHINE_H
