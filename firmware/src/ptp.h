#ifndef PTP_H
#define PTP_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "configuration.h"
#include "definitions.h"                // SYS function prototypes
#include "library/tcpip/udp.h"
#include "config/default/driver/gmac/src/dynamic/drv_gmac_lib.h"
#include "library/tcpip/igmp.h"
#include "custom_drv_gmac.h"
#include "DQAME_V2_0_bsp.h"
#include "timestamp.h"
#include "tsu.h"
#include "state_machine.h"
#include "ethernet_dev.h"
#include "byte_swap.h"
#include "arm_math.h"
#include "get_set_helper.h"

struct PtpMetaData;

struct PtpMetaData *PTP_create();
void PTP_destroy(struct PtpMetaData* ptp);
bool ptp_state_machine_execute(struct PtpMetaData *ptp_data);

#endif // PTP_H
