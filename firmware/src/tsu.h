/* 
 * File:   tsu.h
 * Author: magnu
 *
 * Created on 14. januar 2021, 10:14
 */

#ifndef TSU_H
#define	TSU_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "timestamp.h"
#include "config/default/driver/gmac/src/dynamic/drv_gmac_lib.h"
#include <math.h>
#define PRES_EXTENSION 0 

void tsu_get_time(struct PTP_TIMESTAMP *time, __IO uint32_t TSH, __IO uint32_t TSL, __IO uint32_t TN);
void tsu_set_time(struct PTP_TIMESTAMP *time);
void tsu_comp_set_time(struct PTP_TIMESTAMP *time);
void TSU_adjust(uint32_t nanoseconds, bool subtract);
double get_rate();
double set_rate(double target);
void tsu_set_time2(struct PTP_TIMESTAMP *time);
void tsu_get_time2(struct PTP_TIMESTAMP *time, __IO uint32_t TSH, __IO uint32_t TSL, __IO uint32_t TN);
void tsu_comp_set_time2(struct PTP_TIMESTAMP *time);

#ifdef	__cplusplus
}
#endif

#endif	/* TSU_H */

