/* 
 * File:   timestamp_manipulation.h
 * Author: magnu
 *
 * Created on 14. januar 2021, 10:07
 */

#ifndef TIMESTAMP_MANIPULATION_H
#define	TIMESTAMP_MANIPULATION_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "custom_definitions.h"
#include "definitions.h"                // SYS function prototypes
struct __attribute__((packed)) PTP_TIMESTAMP
{
    uint64_t secondsField;
    uint32_t nanosecondsField;
    bool subtract;
};

struct __attribute__((packed)) PTP_TIMESTAMP_PACKED
{
    uint8_t secondsField[6];
    uint32_t nanosecondsField;
    bool subtract;
};


void time_stamp_subtract(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest);
void time_stamp_add(struct PTP_TIMESTAMP *sourceA, struct PTP_TIMESTAMP *sourceB, struct PTP_TIMESTAMP *dest);
int64_t time_stamp_to_ns(struct PTP_TIMESTAMP sourceA);
#ifdef	__cplusplus
}
#endif

#endif	/* TIMESTAMP_MANIPULATION_H */

