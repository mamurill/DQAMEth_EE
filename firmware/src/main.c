/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "agents/agent_control.h"
#include "ptp.h"

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
//
// THIS MAIN FILE IS NOT COMPLETE AND ONLY SHOWS AN EXAMPLE OF HOW TO SETUP A SOCKER
// USING THE INTERFACE SPECIFIED IN "ethernet_dev.h"
// *****************************************************************************
// *****************************************************************************

struct PtpMetaData* ptp_data;

struct agentExampleObj* agent1;
struct agentPtpLogObj* agent2;

static uint64_t count1 = 0;
static uint64_t count2 = 0;


__attribute__((optimize("O1")))void toggle(void)
{
  uint32_t  random_delay;
  uint8_t   random_delay_2;
  //uint16_t  c_val = 21503; // 75% de las veces se envia
  uint16_t  c_val = 2867; // 10% de las veces se envia

      if (TC2_Timer16bitPeriodGet() < c_val){
        LED_MCU_1_Set();
        //count1++;
      }else{
        LED_MCU_1_Clear();
        //count2++;
      }
}

int main ( void )
{
  //uint32_t random_delay = 0;
    /* Initialize all modules */
    SYS_Initialize ( NULL );


    PAC_PeripheralProtectSetup (PAC_PERIPHERAL_DSU, PAC_PROTECTION_CLEAR);
    


    agent1 = create_example_agent(1400, 7777);
    ptp_data = PTP_create();
    agent2 = create_ptp_log_agent(300, 7778);
    
    start_example_agent(agent1);
    start_ptp_log_agent(agent2);
    SYS_DEBUG_PRINT(SYS_ERROR_INFO, "response 1:\r\n");
        
    while ( true )
    {
      LEMO_Set();
      toggle();
      LEMO_Clear();
      /* Maintain state machines of all polled MPLAB Harmony modules. */
      SYS_Tasks ( );

      ptp_state_machine_execute(ptp_data);
      run_example_agent(agent1);
      run_ptp_log_agent(agent2);
      TC2_Timer16bitPeriodSet(RandomNumber());

    }

    destroy_example_agent(agent1);
    destroy_ptp_log_agent(agent2);
    
    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}




/*******************************************************************************
 End of File
*/

