#include "protocol.h"
#include "math.h"

void process_ingress_data(struct ProtocolMetaData* proto_data);
static void send_frame(struct ProtocolMetaData *protoData, struct Header *header, uint16_t transmitSize);
bool searchTxBuffer(struct ProtocolMetaData *protoData, uint8_t* dest, uint32_t frameNumber);
struct FrameType get_frameType(struct Header *head);
enum FrameControl get_frameControl(struct Header *head);
enum TrafficType get_trafficType(struct Header *head);
uint16_t get_length(struct Header *head);
void set_length(struct Header *head, uint16_t length);
void set_frameType(struct Header *head, enum TrafficType traffic_type, enum FrameControl frame_control);
void set_trafficType(struct Header *head, enum TrafficType type);
void set_frameControl(struct Header *head, enum FrameControl control);
uint32_t get_index(struct Header *head);
void set_time(struct Header *head, uint64_t time);
void set_uid(struct Header *head, uint32_t uid);
void set_crc(crc32 *ptr, crc32 crc);
void set_frameIndex(struct Header *head, uint32_t index);

void connection_event(UDP_SOCKET hUDP,
                      TCPIP_NET_HANDLE hNet,
                      TCPIP_UDP_SIGNAL_TYPE sigType,
                      const void* param)
{
    struct ProtocolMetaData *protoData = (struct ProtocolMetaData *)param;

    switch (sigType)
    {
    case TCPIP_UDP_SIGNAL_RX_DATA:
        protoData->rxQueueLen += 1;
        break;
    case TCPIP_UDP_SIGNAL_TX_DONE:
        break;
    default:
        break;
    }
}

struct StreamingHeader* protocol_write_data(struct ProtocolMetaData* context, uint16_t payloadLength, uint64_t timestamp)
{
    struct ProtocolMetaData *protoData = context;
    uint16_t transmitSize = sizeof(struct Header) + payloadLength + sizeof(crc32);
    set_time(protoData->header[WRITE_BUF], timestamp);
    set_length(protoData->header[WRITE_BUF], transmitSize);
    set_frameType(protoData->header[WRITE_BUF], TRAFFIC_STREAM, FRAME_NORMAL);
    set_frameIndex(protoData->header[WRITE_BUF], protoData->frameCounter);
    protoData->header[WRITE_BUF]->version = PROTOCOL_VERSION;
    fifo_fast_write(protoData->frameBuffer, (void**)&protoData->header[WRITE_BUF]);
    protoData->payload[WRITE_BUF] = (uint8_t* )((uint8_t *)protoData->header[WRITE_BUF] + sizeof(struct Header));
    protoData->frameCounter++;
    return (struct StreamingHeader*)protoData->payload[WRITE_BUF];
}

void protocol_dispatch_queued(struct ProtocolMetaData* protoData)
{
    if (0 < fifo_count(protoData->frameBuffer))
    {
        fifo_read(protoData->frameBuffer, protoData->header[TX_BUF]);
        uint32_t transmitSize = get_length(protoData->header[TX_BUF]);
        send_frame(protoData, protoData->header[TX_BUF], transmitSize);
    }
}

bool protocol_init_socket(struct ProtocolMetaData* proto_data) {

    return eth_run_init(proto_data->conn_data);
}

void process_ingress_data(struct ProtocolMetaData* protoData) 
{
    bool userData = false;

    if(protoData->rxQueueLen)
    {
        protoData->rxQueueLen -= 1;
        read_data(protoData->conn_data, (uint8_t *)protoData->header[RX_BUF], protoData->FrameSize);
        enum TrafficType type = get_trafficType(protoData->header[RX_BUF]);
        enum FrameControl control = get_frameControl(protoData->header[RX_BUF]);
        uint32_t uid = 0;

        if (type == TRAFFIC_STREAM && control == FRAME_NACK)
        {
            uint32_t recIdx = get_index(protoData->header[RX_BUF]);
            uint32_t range = *(uint16_t *)protoData->payload[RX_BUF];
            bswap16(range, &range);
            for (uint16_t i = 0; i < range; i++)
            {
                if (searchTxBuffer(protoData, (uint8_t*)protoData->header[TX_BUF], recIdx + i))
                {
                    set_frameControl(protoData->header[TX_BUF], FRAME_RETRANSMIT);
                    uint32_t transmitSize = get_length(protoData->header[TX_BUF]);
                    send_frame(protoData, protoData->header[TX_BUF], transmitSize);
                }
                else
                {
                    uint32_t transmitSize = sizeof(struct Header) + sizeof(crc32);
                    protoData->header[TX_BUF]->version = PROTOCOL_VERSION;
                    set_uid(protoData->header[TX_BUF], uid);
                    set_length(protoData->header[TX_BUF], transmitSize);
                    set_frameType(protoData->header[TX_BUF], TRAFFIC_STREAM, FRAME_NACK);
                    set_frameIndex(protoData->header[TX_BUF], recIdx + i);
                    send_frame(protoData, protoData->header[TX_BUF], transmitSize);
                }
            }
        }
        else if (type == TRAFFIC_CONTROL && control == FRAME_NORMAL)
        {
            uint32_t recIdx = get_index(protoData->header[RX_BUF]);
            uint32_t transmitSize = sizeof(struct Header) + sizeof(crc32);
            protoData->header[TX_BUF]->version = PROTOCOL_VERSION;
            set_uid(protoData->header[TX_BUF], uid);
            set_length(protoData->header[TX_BUF], transmitSize);
            set_frameType(protoData->header[TX_BUF], TRAFFIC_CONTROL, FRAME_ACK);
            set_frameIndex(protoData->header[TX_BUF], recIdx);
            send_frame(protoData, protoData->header[TX_BUF], transmitSize);

            userData = true;
        }
        else
        {
            userData = true;
        }
        
        if(userData)
        {
            fifo_write(protoData->readFrameBuffer, protoData->header[RX_BUF]);
        }
    }
}

uint32_t protocol_read_data(struct ProtocolMetaData* protoData, struct Header* dest) {
    uint32_t length;
    
    length = fifo_count(protoData->readFrameBuffer);
    
    if(length > 0)
    {
        fifo_read(protoData->readFrameBuffer, dest);
    }
    
    return length;
}

void protocol_run(struct ProtocolMetaData* protoData)
{
    process_ingress_data(protoData);
    protocol_dispatch_queued(protoData);
}

struct ProtocolMetaData *protocol_create(size_t maxFrameBytes, uint16_t port)
{
    struct ProtocolMetaData* protocolData = calloc(1, sizeof(struct ProtocolMetaData));
    uint32_t uid = 0;
    protocolData->frameCounter = 0;
    protocolData-> rxQueueLen = 0;
    protocolData->uid = uid;
    protocolData->conn_data = ConnectionData_create();
    ConnectionData_setup(protocolData->conn_data,
                         port,
                         CLIENT,
                         true,
                         TCPIP_UDP_SIGNAL_RX_DATA,
                         connection_event,
                         protocolData,
                         "192.168.1.2");
//                         "128.141.190.98");

    protocolData->readFrameBuffer = fifo_create(READFRAMEBUFFER_SIZE, maxFrameBytes);
    protocolData->frameBuffer = fifo_create(FRAMEBUFFER_SIZE, maxFrameBytes);
    protocolData->FrameSize = maxFrameBytes;

    protocolData->header[WRITE_BUF] = calloc(1, protocolData->FrameSize);
    protocolData->payload[WRITE_BUF] = (uint8_t* )((uint8_t *)protocolData->header[WRITE_BUF] + sizeof(struct Header));

    protocolData->header[READ_BUF] = calloc(1, protocolData->FrameSize);
    protocolData->payload[READ_BUF] = (uint8_t* )((uint8_t *)protocolData->header[RX_BUF] + sizeof(struct Header));

    protocolData->header[TX_BUF] = calloc(1, protocolData->FrameSize);
    protocolData->payload[TX_BUF] = (uint8_t* )((uint8_t *)protocolData->header[TX_BUF] + sizeof(struct Header));

    protocolData->header[RX_BUF] = calloc(1, protocolData->FrameSize);
    protocolData->payload[RX_BUF] = (uint8_t* )((uint8_t *)protocolData->header[RX_BUF] + sizeof(struct Header));

    set_uid(protocolData->header[WRITE_BUF], uid);
    set_uid(protocolData->header[TX_BUF], uid);

    return protocolData;
}

void protocol_destroy(struct ProtocolMetaData *protocolData)
{
    fifo_release(protocolData->frameBuffer);
    protocolData->frameBuffer = NULL;
    fifo_release(protocolData->readFrameBuffer);
    protocolData->readFrameBuffer = NULL;
    
    if (protocolData->header[READ_BUF] != NULL)
    {
        free(protocolData->header[READ_BUF]);
    }
    protocolData->header[READ_BUF] = NULL;
    protocolData->payload[READ_BUF] = NULL;

    if (protocolData->header[RX_BUF] != NULL)
    {
        free(protocolData->header[RX_BUF]);
    }
    protocolData->header[RX_BUF] = NULL;
    protocolData->payload[RX_BUF] = NULL;

    if (protocolData->header[WRITE_BUF] != NULL)
    {
        free(protocolData->header[WRITE_BUF]);
    }
    protocolData->header[WRITE_BUF] = NULL;
    protocolData->payload[WRITE_BUF] = NULL;

    if (protocolData->header[TX_BUF] != NULL)
    {
        free(protocolData->header[TX_BUF]);
    }
    protocolData->header[TX_BUF] = NULL;
    protocolData->payload[TX_BUF] = NULL;

    if (protocolData != NULL)
    {
        free(protocolData);
    }
}

static void send_frame(struct ProtocolMetaData *protoData, struct Header *header, uint16_t transmitSize)
{
    uint16_t payloadLength = transmitSize - sizeof(struct Header) - sizeof(crc32);
    crc32 crc = 0;
    crc32 *CrcPtr = (crc32 *)(((uint8_t *)header) + sizeof(struct Header) + payloadLength);
    set_crc(CrcPtr, crc);
    uint32_t crcAddr = ((uint32_t)&header->length) >> 2;
    uint32_t crcWords = (transmitSize - sizeof(crc32) + 3) >> 2; // Padding bytes for CRC HW
    DSU_REGS->DSU_ADDR = DSU_ADDR_ADDR(crcAddr);
    DSU_REGS->DSU_LENGTH = DSU_LENGTH_LENGTH(crcWords);
    DSU_REGS->DSU_DATA = DSU_DATA_DATA(0xFFFFFFFF);
    DSU_REGS->DSU_CTRL = DSU_CTRL_CRC(1);
    while (!(DSU_REGS->DSU_STATUSA & DSU_STATUSA_DONE_Msk))
    {
    }

    if (!(DSU_REGS->DSU_STATUSA & DSU_STATUSA_BERR_Msk))
    {
        DSU_REGS->DSU_STATUSA = 0x1F;
        crc32 crc32HW = (uint32_t)DSU_REGS->DSU_DATA;
        crc32HW = ~crc32HW;
        crc32 *ptr = (crc32 *)(((uint8_t *)header) + sizeof(struct Header) + payloadLength); //Pointer to CRC32 field
        set_crc(ptr, crc32HW);
        transmit_data(protoData->conn_data, (uint8_t *)header, transmitSize);
    }
}

 /* Searching to be reviewed in order to optimize it */
bool searchTxBuffer(struct ProtocolMetaData *protoData, uint8_t* dest, uint32_t frameNumber)
{
    bool sucess = false;
    for (uint32_t i = 0; protoData->frameBuffer->depth; i++)
    {
        uint32_t index = get_index((struct Header*)fifo_peek(protoData->frameBuffer, i));
        if (index == frameNumber)
        {
            sucess = true;
            fifo_browse(protoData->frameBuffer, dest, i);
            break;
        }
    }
    return sucess;
}

uint16_t get_length(struct Header *head)
{
    return get_2byte(head->length, PROTOCOL_BIG_ENDIAN);
}

void set_type(struct Header *head, uint8_t type){
    set_byte(&head->type, type);
}

void set_length(struct Header *head, uint16_t length)
{
    set_2byte(&head->length, length, PROTOCOL_BIG_ENDIAN);
}

//I think this is not the most efficient. Should be rewritten
struct FrameType get_frameType(struct Header *head)
{
    struct FrameType type;
    type.frameControl = (head->type & 0xBC) >> 2;
    type.trafficType = (head->type & 0x03);
    return type;
}

enum FrameControl get_frameControl(struct Header *head)
{
    enum FrameControl control = (enum FrameControl)(head->type & 0x03);
    return control;
}

enum TrafficType get_trafficType(struct Header *head)
{
    enum TrafficType type = (enum TrafficType)((head->type & 0xBC) >> 2);
    return type;
}

void set_frameType(struct Header *head, enum TrafficType traffic_type, enum FrameControl frame_control)
{
    head->type = ((traffic_type << 2) & 0xFC) + (frame_control & 0x03);
}

void set_frameControl(struct Header *head, enum FrameControl control)
{
    head->type = head->type & 0xFC; //Reset frame control bits
    head->type = head->type + (control & 0x03);
}

void set_trafficType(struct Header *head, enum TrafficType type)
{
    head->type = head->type & 0x03; //Reset traffic type bits
    head->type = head->type + ((type & 0xFC) << 2) ;
}

uint32_t get_uid(struct Header *head)
{
    return get_4byte(head->uid, PROTOCOL_BIG_ENDIAN);
}

void set_uid(struct Header *head, uint32_t uid)
{
    set_4byte(&head->uid, uid, PROTOCOL_BIG_ENDIAN);
}

uint32_t get_index(struct Header *head)
{
    return get_4byte(head->index, PROTOCOL_BIG_ENDIAN);
}

void set_frameIndex(struct Header *head, uint32_t index)
{
    set_4byte(&head->index,index, PROTOCOL_BIG_ENDIAN);
}

void set_time(struct Header *head, uint64_t time)
{
    set_8byte(&head->time, time, PROTOCOL_BIG_ENDIAN);
}

void set_crc(crc32 *ptr, crc32 crc)
{
    set_4byte(ptr,crc, PROTOCOL_BIG_ENDIAN);
}