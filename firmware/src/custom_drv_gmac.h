/* 
 * File:   custom_drv_gmac_local.h
 * Author: magnu
 *
 * Created on 14. januar 2021, 13:06
 */

#ifndef CUSTOM_DRV_GMAC_LOCAL_H
#define	CUSTOM_DRV_GMAC_LOCAL_H


#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "system_config.h"
#include "system/system.h"
#include "system/int/sys_int.h"
#include "system/time/sys_time.h"
#include "tcpip/tcpip_mac_object.h"

#include "driver/gmac/drv_gmac.h"
#include "driver/gmac/src/dynamic/drv_gmac_lib.h"

GMAC_EVENTS _XtlEventsTcp2Eth(TCPIP_MAC_EVENT tcpEv);
extern DRV_GMAC_DRIVER _gmac_drv_dcpt[1];
TCPIP_MAC_EVENT _XtlEventsEth2Tcp(GMAC_EVENTS eEvents);
DRV_GMAC_DRIVER* _GmacHandleToMacInst(uintptr_t handle);

// *****************************************************************************
/* PTP Event Flags - CUSTOM ENUM

  Summary:
    PTP event flags

  Description:
    This enumeration defines flags for the possible PTP events that can
    cause interrupts.
*/
typedef enum{
    // All events
	GMAC_EV_PTP_NONE = 0,

    GMAC_EV_PTP_DRQFR = GMAC_ISR_DRQFR_Msk,
            
    GMAC_EV_PTP_SFR = GMAC_ISR_SFR_Msk,
            
    GMAC_EV_PTP_DRQFT = GMAC_ISR_DRQFT_Msk,
            
    GMAC_EV_PTP_SFT = GMAC_ISR_SFT_Msk,
            
    GMAC_EV_PTP_PDRQFR = GMAC_ISR_PDRQFR_Msk,
            
    GMAC_EV_PTP_PDRSFR =GMAC_ISR_PDRSFR_Msk,
            
    GMAC_EV_PTP_PDRQFT = GMAC_ISR_PDRQFT_Msk,
            
    GMAC_EV_PTP_PDRSFT = GMAC_ISR_PDRSFT_Msk,
            
    GMAC_EV_PTP_SRI = GMAC_ISR_SRI_Msk,
            
    GMAC_EV_PTP_TSUCMP = GMAC_ISR_TSUCMP_Msk,
            
	// All events
	GMAC_EV_PTP_ALL = (GMAC_EV_PTP_DRQFR | GMAC_EV_PTP_SFR | GMAC_EV_PTP_DRQFT |
    GMAC_EV_PTP_SFT | GMAC_EV_PTP_PDRQFR | GMAC_EV_PTP_PDRSFR | GMAC_EV_PTP_PDRQFT |
    GMAC_EV_PTP_PDRSFT | GMAC_EV_PTP_SRI | GMAC_EV_PTP_TSUCMP)

            
} GMAC_PTP_EVENTS;

typedef void    (*PTP_EVENT_HANDLER)(GMAC_PTP_EVENTS event, void* context);

void setPTPEvent(PTP_EVENT_HANDLER, void*);


#endif	/* CUSTOM_DRV_GMAC_LOCAL_H */

