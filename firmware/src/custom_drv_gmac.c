#include "custom_drv_gmac.h"


PTP_EVENT_HANDLER ptp_event = NULL;
void* context = NULL;

/*******************************************************************************
  Function:
    bool    DRV_GMAC_EventMaskSet(DRV_HANDLE hMac, TCPIP_MAC_EVENT macEvents, bool enable)

  Summary:
    Enables the MAC events.

  Description:
     This function sets the enabled events.
     Multiple events can be orr-ed together.
     All events that are set will be added to the notification process. The old events will be disabled.
     The stack (or stack user) has to catch the events that are notified and process them:
         - The stack should process the TCPIP_MAC_EV_RX_PKTPEND/TCPIP_MAC_EV_RX_DONE, TCPIP_MAC_EV_TX_DONE transfer events
         - Process the specific condition and acknowledge them calling DRV_GMAC_EventAcknowledge() so that they can be re-enabled.

  Precondition:
   DRV_GMAC_EventInit should have been called.

  Parameters:
    hMac      - parameter identifying the intended MAC  
    macEvMask - events the user of the stack wants to add/delete for notification
    enable    - if true, the events will  be enabled, else disabled
    
  Returns:
    true  if operation succeeded,
    an error code otherwise

  Example:
    <code>
    DRV_GMAC_EventMaskSet( hMac, TCPIP_MAC_EV_RX_OVFLOW | TCPIP_MAC_EV_RX_BUFNA, true );
    </code>

  Remarks:
    The event notification system enables the user of the TCPIP stack to call into the stack
    for processing only when there are relevant events rather than being forced to periodically call
    from within a loop.
    
    If the notification events are nill the interrupt processing will be disabled.
    Otherwise the event notification will be enabled and the interrupts relating to the requested events will be enabled.
    
    Note that once an event has been caught by the stack ISR (and reported if a notification handler is in place)
    it will be disabled until the DRV_GMAC_EventAcknowledge() is called.

*****************************************************************************/
bool DRV_GMAC_EventMaskSet(DRV_HANDLE hMac, TCPIP_MAC_EVENT macEvMask, bool enable) 
{
	DRV_GMAC_DRIVER * pMACDrv = _GmacHandleToMacInst(hMac);
    if(pMACDrv == 0)
    {
        return false;
    }
  uint16_t test = sizeof(GMAC_EVENTS);
  test++;
  test--;
	DRV_GMAC_EVENT_DCPT*  pDcpt = &pMACDrv->sGmacData._gmac_event_group_dcpt;	
	GMAC_QUE_LIST queIdx;
    GMAC_EVENTS ethEvents;
	
	if(enable)
	{
		GMAC_EVENTS  ethSetEvents;
        
		ethSetEvents = _XtlEventsTcp2Eth(macEvMask);

		if(pDcpt->_TcpEnabledEvents != 0)
		{   // already have some active
            DRV_PIC32CGMAC_LibSysInt_Disable(pMACDrv, GMAC_ALL_QUE_MASK, NULL);
		}

		pDcpt->_TcpEnabledEvents |= macEvMask;        // add more
		pDcpt->_EthEnabledEvents |= ethSetEvents;

		if(pDcpt->_TcpEnabledEvents != 0)
		{
			ethSetEvents &= ~pDcpt->_EthPendingEvents;		// keep just the new un-ack events			
            for(queIdx = GMAC_QUE_0; queIdx < DRV_GMAC_NUMBER_OF_QUEUES; queIdx++)
            {
                ethEvents = ethSetEvents;    
                if(pMACDrv->sGmacData.gmacConfig.gmac_queue_config[queIdx].queueTxEnable != true)
                {
                    ethEvents = ethEvents & (~GMAC_EV_TX_ALL);
                }
                if(pMACDrv->sGmacData.gmacConfig.gmac_queue_config[queIdx].queueRxEnable != true)
                {
                    ethEvents = ethEvents & (~GMAC_EV_RX_ALL);
                }
                //Read ISR register to clear the interrupt status	
                DRV_PIC32CGMAC_LibReadInterruptStatus(queIdx);
                //Enable GMAC interrupts
                //DRV_PIC32CGMAC_LibEnableInterrupt(queIdx, ethEvents);
                GMAC_REGS->GMAC_IER = ethEvents | (GMAC_EV_PTP_TSUCMP | GMAC_EV_PTP_SRI | GMAC_EV_PTP_DRQFT);

            }
            
            DRV_PIC32CGMAC_LibSysInt_Enable(pMACDrv, GMAC_ALL_QUE_MASK);
		}
	}
	else
	{   // disable some events
		GMAC_EVENTS  ethClrEvents;
        bool intStat[DRV_GMAC_NUMBER_OF_QUEUES];

		macEvMask &= pDcpt->_TcpEnabledEvents;                  // keep just the enabled ones
		ethClrEvents = _XtlEventsTcp2Eth(macEvMask);

		if(pDcpt->_TcpEnabledEvents != 0)
		{   // already have some active
            DRV_PIC32CGMAC_LibSysInt_Disable(pMACDrv, GMAC_ALL_QUE_MASK, intStat);
		}

		pDcpt->_TcpEnabledEvents &= ~macEvMask;     // clear some of them
		pDcpt->_EthEnabledEvents &= ~ethClrEvents;

		pDcpt->_TcpPendingEvents &= ~macEvMask;     // remove them from un-ack list
		pDcpt->_EthPendingEvents &= ~ethClrEvents;
		
        for(queIdx = GMAC_QUE_0; queIdx < DRV_GMAC_NUMBER_OF_QUEUES; queIdx++)
        {            
            //Disable GMAC interrupts
            DRV_PIC32CGMAC_LibDisableInterrupt(queIdx, ethClrEvents);
            //Read ISR register to clear the interrupt status	
            DRV_PIC32CGMAC_LibReadInterruptStatus(queIdx);

        }

		if(pDcpt->_TcpEnabledEvents != 0)
		{
            DRV_PIC32CGMAC_LibSysInt_Restore(pMACDrv, GMAC_ALL_QUE_MASK, intStat);
		}
	}

	return true;

}

/****************************************************************************
 * Function:        DRV_GMAC_Tasks_ISR
 *
 * PreCondition:    DRV_GMAC_EventInit, DRV_GMAC_EventMaskSet should have been called.
 *
 * Input:           macIndex - PIC32 MAC object index
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function processes the Ethernet interrupts and reports the events back to the user.
 *
 * Note:            None
 ******************************************************************************/
void DRV_GMAC_Tasks_ISR( SYS_MODULE_OBJ macIndex, uint32_t  currEthEvents)
{
  GMAC_EVENTS currGroupEvents = (GMAC_EVENTS)GMAC_EV_NONE;
  GMAC_PTP_EVENTS currPtpEvents = (GMAC_PTP_EVENTS) GMAC_EV_PTP_NONE;
	DRV_GMAC_EVENT_DCPT* pDcpt;	
	DRV_GMAC_DRIVER * pMACDrv = &_gmac_drv_dcpt[macIndex];		

	// process interrupts
	pDcpt = &pMACDrv->sGmacData._gmac_event_group_dcpt;
    //  keep just the relevant ones
	currGroupEvents = ((GMAC_EVENTS)currEthEvents) & pDcpt->_EthEnabledEvents;
  currPtpEvents = ((GMAC_PTP_EVENTS)currEthEvents) & GMAC_EV_PTP_ALL;
    
  if(currPtpEvents)
  {
    ptp_event(currPtpEvents, context);
  }
	if(currGroupEvents)
	{
        // add the new events
		pDcpt->_EthPendingEvents |= currGroupEvents;                    
		pDcpt->_TcpPendingEvents |= _XtlEventsEth2Tcp(currGroupEvents);

		if(pDcpt->_TcpNotifyFnc)
		{
            // let the user know
			(*pDcpt->_TcpNotifyFnc)(pDcpt->_TcpPendingEvents, pDcpt->_TcpNotifyParam);     
		}
	}

}

void setPTPEvent(PTP_EVENT_HANDLER event, void* content)
{
    ptp_event = event;
    context = content;
}