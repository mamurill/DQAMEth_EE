#ifndef CUSTOM_GMAC_LIB_SAME5X_H
#define CUSTOM_GMAC_LIB_SAME5X_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "system_config.h"
#include "system/system.h"
#include "system/int/sys_int.h"
#include "system/time/sys_time.h"
#include "tcpip/tcpip_mac_object.h"

#include "driver/gmac/drv_gmac.h"
#include "driver/gmac/src/dynamic/drv_gmac_lib.h"

extern uint32_t drvGmacQueEvents;

#endif // CUSTOM_GMAC_LIB_SAME5X_H