#include "crc32.h"

uint32_t crc32b(const uint8_t *frame, uint16_t length) {
    
    int32_t i, j;
    uint32_t crc, mask;

    crc = 0xFFFFFFFF;
    for (i = 0; i < length; i++) {    
        crc ^= (uint32_t)(frame[i]);
        for (j = 7; j >= 0; j--) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
    }
    
    return ~crc;
}