/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef ETHERNET_DEV_H /* Guard against multiple inclusion */
#define ETHERNET_DEV_H

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "definitions.h" // SYS function prototypes
#include "library/tcpip/udp.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C"
{
#endif

  /** ConnectionTypes

    @Summary
      Enum of different connection types.     
    
    @Description
      The Microchip UDP Library supports the creation of Client and Server socket.
      This enum defines the two types. 
     
    @Remarks
        
  */
  enum ConnectionTypes
  {
    CLIENT,
    SERVER
  };


  /** ConnectionData Struct

    @Summary
      Forward declaration of the ConnectionData struct.    
    
    @Description
      Forward declaration of the ConnectionData struct. This allows other modules to see the existance of the data type,
      however its content is obfuscated and hidden in the corresponding .c file
     
    @Remarks
        
  */
  struct ConnectionData;


  /**
    @Function
      struct ConnectionData* ConnectionData_create();

    @Summary
      creates a ConnectionData struct. 

    @Description
      Allocates memory for a ConnectionData struct and returns a pointer to the struct. 
           
    @Precondition
      None

    @Parameters
      None

    @Returns
      struct ConnectionData*
    @Remarks
  */
  struct ConnectionData* ConnectionData_create();


  /**
    @Function
      void ConnectionData_destroy(struct ConnectionData *conn_data);

    @Summary
      Destroys a ConnectionData struct. 

    @Description
      Frees the memory allocated for ConnectionData struct. 
           
    @Precondition
      None

    @Parameters
      @param conn_data Pointer to a ConnectionData struct

    @Returns
      None
    @Remarks
  */
  void ConnectionData_destroy(struct ConnectionData *conn_data);


  /**
    @Function
      bool ConnectionData_setup(struct ConnectionData *conn_data, UDP_PORT port, enum ConnectionTypes client, TCPIP_UDP_SIGNAL_HANDLE event, char *ipStr);

    @Summary
      Initializes a ConnectionData struct. 

    @Description
      Initializes a ConnectionData struct with the relevant udp port, connection type, host IP, and event handler function
           
    @Precondition
      None

    @Parameters
      @param conn_data Pointer to a ConnectionData struct
      @param port UDP port of the connection
      @param connectionType Type of connection (SERVER or CLIENT)
      @param event Pointer to an eventhandler 
      @param ipStr Pointer to a null terminated ipV4 string
    @Returns
      bool True if setup success, False if something failed
    @Remarks
  */
bool ConnectionData_setup(struct ConnectionData *conn_data, UDP_PORT port, enum ConnectionTypes connectionType,bool enableSignal, TCPIP_UDP_SIGNAL_TYPE interruptSignals, TCPIP_UDP_SIGNAL_FUNCTION event, void* context, char *ipStr);


  /**
    @Function
      bool state_machine_poll(struct ConnectionData *conn_data);

    @Summary
      Runs the state machine required to initialize the connection

    @Description
      To initialise the connection a number of steps must be carried out in succession. Some of these steps requires an unknown amount of time to elapse
      before the next step can be started. Everytime this state machine function is called, it either runs one of these steps or checks if it has completed.
      When all steps have succeeded the connection has been established and the function returns True.
           
    @Precondition
      ConnectionData_setup() must have been called succesfully

    @Parameters
      @param conn_data Pointer to a ConnectionData struct
    @Returns
      bool Returns True when connection initialisation is complete. Otherwise False
    @Remarks
  */  
  bool eth_state_machine_execute(struct ConnectionData *conn_data);


    /**
    @Function
      bool eth_run_init(struct ConnectionData *conn_data)

    @Summary
      Runs the initialization process required for the connection

    @Description
      To initialise the connection a number of steps must be carried out in succession. Some of these steps requires an unknown amount of time to elapse
      before the next step can be started. Everytime this function is called, it either runs one of these steps or checks if it has completed.
      When all steps have succeeded the connection has been established and the function returns True.
           
    @Precondition
      ConnectionData_setup() must have been called succesfully

    @Parameters
      @param conn_data Pointer to a ConnectionData struct
      
    @Returns
      bool Returns True when connection initialisation is complete. Otherwise False
    @Remarks
  */  
  bool eth_run_init(struct ConnectionData *conn_data);


  UDP_SOCKET get_socket(struct ConnectionData *conn_data);

  /**
    @Function
      uint16_t transmit_data(struct ConnectionData *conn_data, const uint8_t *cData, uint16_t wDataLen);

    @Summary
      Transmits data.

    @Description
      Transmits a byte array on a previously established UDP connection.
           
    @Precondition
      state_machine_poll() must have returned True

    @Parameters
      @param conn_data Pointer to a ConnectionData struct
      @param cData Pointer to the data to transmit
      @param wDataLen Number of bytes to transmit
    @Returns
      uint16_t Returns the number of bytes transmitted. If the transmission failed, 0 is returned.
    @Remarks
  */  
  uint16_t transmit_data(struct ConnectionData *conn_data, const uint8_t *cData, uint16_t wDataLen);

  uint16_t read_data(struct ConnectionData *conn_data, uint8_t* dest, uint16_t size);

  uint16_t discard_data(struct ConnectionData *conn_data);

  /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* ETHERNET_DEV_H */

/* *****************************************************************************
 End of File
 */
