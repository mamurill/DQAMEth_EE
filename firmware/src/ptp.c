#include "ptp.h"
#include "agent_ptp_log.h"

#define PTP_CRITICAL_EVENT_PORT 319  //Defines the port where PTP event packets (timestamped packets) arrive
#define PTP_GENERAL_MESSAGE_PORT 320 //Defines the port where PTP general packets (non timestamped packets) arrive
#define ONE_STEP_MODE 0
#define BILLION_D 1000000000.0
#define PTP_BIG_ENDIAN 1
extern struct AgentPtpLogAcquisition global_ptp_data;
extern bool ptpDataReady;


enum __attribute__((packed)) PTP_MESSAGE_TYPE
{
    SYNC = 0,
    DELAY_REQ,
    PDELAY_REQ,
    PDELAY_RESP,

    FOLLOW_UP = 8,
    DELAY_RESP,
    PDELAY_RESP_FOLLOW_UP,
    ANNOUNCE,
    SIGNALING,
    MANAGEMENT
};

enum __attribute__((packed)) PTP_VERSION
{
    PTP_VERSION_1 = 1,
    PTP_VERSION_2
};

enum __attribute__((packed)) PTP_HEADER_FLAGS
{
    ALTERNATE_MASTER_FLAG = (1 << 8),
    TWO_STEP_FLAG = (1 << 9),
    UNICAST_FLAG = (1 << 10),
    PTP_PROFILE_SPECIFIC_1 = (1 << 13),
    PTP_PROFILE_SPECIFIC_2 = (1 << 14),
    RESERVED = (1 << 15),
    LEAP61 = (1 << 0),
    LEAP69 = (1 << 1),
    CURRENT_UTC_OFFSET_VALID = (1 << 2),
    PTP_TIMESCALE = (1 << 3),
    TIME_TRACEABLE = (1 << 4),
    FREQUENCY_TRACEABLE = (1 << 5)
};

#pragma pack(1)
struct __attribute__((packed)) CLOCK_ID
{
    uint8_t OUI[3];  //OUI is actually 24 bits
    uint8_t UUID[5]; //UUID is actually 40 bits?
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) PORT_IDENTITY
{
    struct CLOCK_ID clockID;
    uint16_t portNumber;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((__packed__)) transportSpecificMessageTypeBitField
{
    uint8_t MessageType : 4, transportSpecific : 4; //Can not use enum in definition due to bit field limitations
};
#pragma pack()

#pragma pack(1)
struct __attribute__((__packed__)) PTP_HEADER
{
    struct transportSpecificMessageTypeBitField transportSpecificMessageType;
    enum PTP_VERSION versionPTP;
    uint16_t messageLength;
    uint8_t domainNumber;
    uint8_t reserved;
    enum PTP_HEADER_FLAGS flagField;
    uint64_t correctionField;
    uint32_t reserved2;
    struct PORT_IDENTITY sourcePortIdentity; //Fix This
    uint16_t sequenceId;
    uint8_t controlField;
    uint8_t logMessageInterval;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) PTP_SYNC_DELAY_REQ
{
    struct PTP_TIMESTAMP_PACKED originTimestamp;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) PTP_FOLLOW_UP
{
    struct PTP_TIMESTAMP_PACKED preciseOriginTimestamp;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) PTP_DELAY_RESP
{
    struct PTP_TIMESTAMP_PACKED receiveTimestamp;
    struct PORT_IDENTITY requestingPortIdentity;
};
#pragma pack()

#pragma pack(1)
union __attribute__((packed)) PTP_MESSAGE_BODY
{
    struct PTP_SYNC_DELAY_REQ ptpSyncDelayReq;
    struct PTP_FOLLOW_UP ptpFollowUp;
    struct PTP_DELAY_RESP ptpDelayResp;
};
#pragma pack()

#pragma pack(1)
struct __attribute__((packed)) PTP_MESSAGE
{
    struct PTP_HEADER header;
    union PTP_MESSAGE_BODY body;
};
#pragma pack()

struct PTP_TRANSACTION_TIMESTAMPS
{
    struct PTP_TIMESTAMP t1;
    struct PTP_TIMESTAMP t2;
    struct PTP_TIMESTAMP t3;
    struct PTP_TIMESTAMP t4;
    struct PTP_TIMESTAMP s2;
    struct PTP_TIMESTAMP s4;
};

struct PTP_DIFF_S
{
    float dm2s;
    float ds2m;
    float dprop;
    float offset;
    float filteredOffset;
};

// *****************************************************************************

enum StateMachineStates
{
    /* Application's state machine's initial state. */
    PTP_STATE_ERROR = ST_ERROR,
    PTP_STATE_NULL = ST_NULL,
    PTP_STATE_TERM = ST_TERM,
    PTP_STATE_ANY = ST_ANY,
    PTP_STATE_INIT,
    PTP_STATE_WAIT_FOLLOW_UP,
	PTP_STATE_WAIT_INTERRUPT,						 
    PTP_STATE_WAIT_DELAY_REQ,
    PTP_STATE_WAIT_DELAY_RES,
    PTP_STATE_IDLE
};

enum StateMachineEvents
{
    PTP_EV_NONE = EV_NONE,
    PTP_EV_ANY = EV_ANY,
    PTP_EV_MSG_EVT,
    PTP_EV_MSG_GNR,
    PTP_EV_TSU,
    PTP_EV_SYNC,
    PTP_EV_FOLLOW_UP,
	PTP_EV_TIMER_INTERRUPT,					   
    PTP_EV_DELAY_RESPONSE
};

//Forward Declaration of State Machine Functions
static uint32_t init_eth(struct PtpMetaData *ptpData);
static uint32_t idle(struct PtpMetaData *ptpData);
static uint32_t decode_msg_evt(struct PtpMetaData *ptpData);
static uint32_t decode_msg_gnr(struct PtpMetaData *ptpData);
static uint32_t sync(struct PtpMetaData *ptpData);
static uint32_t follow_up(struct PtpMetaData *ptpData);
static uint32_t delay_req_TSU(struct PtpMetaData *ptpData);
static uint32_t delay_resp(struct PtpMetaData *ptpData);
static uint32_t send_delay_request(struct PtpMetaData *ptpData);
static uint16_t delay_req_counter = 0;																


struct TransitionPTPObj
{
    struct TransitionObj transistionConditions;
    uint32_t (*fn)(struct PtpMetaData *);
};

static struct TransitionPTPObj trans[] = {
    {{PTP_STATE_INIT, PTP_EV_NONE}, &init_eth},
    {{PTP_STATE_IDLE, PTP_EV_NONE}, &idle},
    {{PTP_STATE_ANY, PTP_EV_MSG_EVT}, &decode_msg_evt},
    {{PTP_STATE_ANY, PTP_EV_MSG_GNR}, &decode_msg_gnr},
    {{PTP_STATE_ANY, PTP_EV_SYNC}, &sync},
    {{PTP_STATE_WAIT_FOLLOW_UP, PTP_EV_FOLLOW_UP}, &follow_up},
	{{PTP_STATE_WAIT_INTERRUPT, PTP_EV_TIMER_INTERRUPT}, &send_delay_request},																		  
    {{PTP_STATE_WAIT_DELAY_REQ, PTP_EV_TSU}, &delay_req_TSU},
    {{PTP_STATE_WAIT_DELAY_RES, PTP_EV_DELAY_RESPONSE}, &delay_resp},
};

static const uint32_t TRANS_COUNT = (sizeof(trans) / sizeof(*trans));


uint32_t execute_ptp_state_machine(struct StateMachineMetaData *obj, uint32_t idx)
{
    struct PtpMetaData *ptpData = (struct PtpMetaData *)obj; //Downcast StateMachineMetaData
    return trans[idx].fn(ptpData);
}

// *****************************************************************************

/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

struct PtpMetaData
{
    struct StateMachineMetaData stateMachine; //should not be pointer
    struct ConnectionData *udpPtpEvtClient;
    struct ConnectionData *udpPtpGnrClient;

    struct PTP_TIMESTAMP comparison;
    struct PTP_TIMESTAMP comparisonAdd;

    struct PTP_MESSAGE ptpMsgRec;
    struct PTP_MESSAGE ptpMsgTrans;
    bool timeInit;
    bool delayReqSend;
    struct PTP_TIMESTAMP time;
    bool syncTimeStampSet;
    struct PTP_TRANSACTION_TIMESTAMPS timestamps;
    struct PTP_TIMESTAMP SyncCorrection;
    arm_pid_instance_f32 pid;
    float time_offset_est;
    double desired_rate;
    double set_rate;
    /* TODO: Define any additional data used by the application. */
};

static void run_PID(struct PtpMetaData *ptpData);
void set_transport_specific(struct PtpMetaData *ptpData, uint8_t transport);
void set_message_type(struct PtpMetaData *ptpData, enum PTP_MESSAGE_TYPE message);
void set_ptp_version(struct PtpMetaData *ptpData, enum PTP_VERSION versionPTP);
void set_message_length(struct PtpMetaData *ptpData, uint16_t length);
void set_domain_number(struct PtpMetaData *ptpData, uint8_t domain);
void set_flags(struct PtpMetaData *ptpData, enum PTP_HEADER_FLAGS flags);
void set_correction(struct PtpMetaData *ptpData, uint64_t correction);
void set_port_id_oui(struct PtpMetaData *ptpData, uint32_t oui);
void set_port_id_uuid(struct PtpMetaData *ptpData, uint64_t uuid);
void set_port_id_port_number(struct PtpMetaData *ptpData, uint16_t portNumber);
void set_sequence_id(struct PtpMetaData *ptpData, uint16_t sequenceId);
void set_control_field(struct PtpMetaData *ptpData, uint8_t controlField);
void set_log_message_interval(struct PtpMetaData *ptpData, uint8_t logMessageInterval);
void delay_request_interrupt(TC_TIMER_STATUS status, uintptr_t context);																		

/*******************************************************************
 * NAME :            void PTP_Packet_Received_Event_Handler(UDP_SOCKET hUDP,
                                       TCPIP_NET_HANDLE hNet,
                                       TCPIP_UDP_SIGNAL_TYPE sigType,
                                       const void* param)
 *
 * DESCRIPTION :     Event handler called whenever a PTP packet is received over UDP.
 *                   The handler is bound to a socket using TCPIP_UDP_SignalHandlerRegister.
 *                   The function must execute very quickly and thus only notifies the main PTP thread of the event that has occurred 
 *
 * INPUTS :
 *       PARAMETERS:
 *           UDP_SOCKET              hUDP                Handle of the UDP socket that received the packet          
 *           TCPIP_NET_HANDLE        hNet                Network interface that received the packet         
 *           TCPIP_UDP_SIGNAL_TYPE   sigType             Describes the event that occurred  
 *           const void*             param               User supplied parameter. Not currently supported and will always be NULL              
 * OUTPUTS :
 *       RTOS Signals:
 *           UDP_SOCKET              hUDP                
 * PROCESS :
 *                   [1]  Enters switch case based on the type of event that occurred
 *                   [2]     Notifies the PTP application thread which even that has occurred
 *
 * NOTES :           Only TCPIP_UDP_SIGNAL_RX_DATA are currently processed
 */
void PTP_Packet_Received_Event_Handler(UDP_SOCKET hUDP,
                                       TCPIP_NET_HANDLE hNet,
                                       TCPIP_UDP_SIGNAL_TYPE sigType,
                                       const void *param)
{
    struct PtpMetaData *ptpData = (struct PtpMetaData *)param;
    EventObj event;
    switch (sigType)
    {
    case TCPIP_UDP_SIGNAL_RX_DATA:
        if (hUDP == get_socket(ptpData->udpPtpEvtClient))
        {
            event = PTP_EV_MSG_EVT;
            Queue_event(&ptpData->stateMachine, &event);
        }
        else if (hUDP == get_socket(ptpData->udpPtpGnrClient))
        {
            event = PTP_EV_MSG_GNR;
            Queue_event(&ptpData->stateMachine, &event);
        }
        break;
    case TCPIP_UDP_SIGNAL_TX_DONE:
        break;
    default:
        break;
    }
}

/*******************************************************************
 * NAME :            PTP_Event_Handler(GMAC_PTP_EVENTS event)
 *
 * DESCRIPTION :     Event handler called whenever a PTP event (as defined by GMAC_PTP_EVENTS) occurs in the GMAC.
 *                   The handler is bound to the GMAC using setPTPEvent.
 *
 * INPUTS :
 *       PARAMETERS:
 *           GMAC_PTP_EVENTS             event           Describes the event that occurred                         
 * OUTPUTS :
 *       RTOS Signals:
 *           UDP_CLIENT_APP_EVENTS       tsuEvent        used to indicate that a "time stamp unit" event has occurred    
 * PROCESS :
 *                   [1]  Enters switch case based on the type of event that occurred
 *                   [2]     behaviour now depends on type of event that occurred
 *
 * NOTES :           Only GMAC_ISR_DRQFT_Msk and GMAC_ISR_SRI_Msk are currently processed
 */
void PTP_Event_Handler(GMAC_PTP_EVENTS event, void *param)
{
    struct PtpMetaData *ptpData = (struct PtpMetaData *)param;
    EventObj eventObj;

    //PTP Delay Request Frame Received
    if (event & GMAC_ISR_DRQFR_Msk)
    {
    }

    //PTP Sync Frame Received
    if (event & GMAC_ISR_SFR_Msk)
    {
    }

    //PTP Delay Request Frame Transmitted
    if (event & GMAC_ISR_DRQFT_Msk)
    {
        eventObj= PTP_EV_TSU;
        Queue_event(&ptpData->stateMachine, &eventObj);
        //vTaskNotifyGiveFromISR(xPTP_APP_Tasks, NULL); //Inform application that the delay request has occurred
        //xTaskNotifyFromISR(xPTP_APP_Tasks, TSU_STAMP << 16, eSetValueWithOverwrite, NULL);
    }

    //PTP Sync Frame Transmitted
    if (event & GMAC_ISR_SFT_Msk)
    {
        //vTaskNotifyGiveFromISR(xPTP_APP_Tasks, NULL);
    }

    //PDelay Request Frame Received
    if (event & GMAC_ISR_PDRQFR_Msk)
    {
    }

    //PDelay Response Frame Received
    if (event & GMAC_ISR_PDRSFR_Msk)
    {
    }

    //PDelay Request Frame Transmitted
    if (event & GMAC_ISR_PDRQFT_Msk)
    {
    }

    //PDelay Response Frame Transmitted
    if (event & GMAC_ISR_PDRSFT_Msk)
    {
    }

    //TSU Seconds Register Increment, Indicates the register has incremented
    if (event & GMAC_ISR_SRI_Msk)
    {
    }

    //TSU Timer Comparison, Indicates TSU times count and comparison value are equal
    if (event & GMAC_ISR_TSUCMP_Msk)
    {
        //time_stamp_add(&ptp_appData.comparison,&ptp_appData.comparisonAdd,&ptp_appData.comparison);
        //tsu_comp_set_time2(&ptp_appData.comparison);
        //TC2_TimerStart();
    }
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

enum PTP_MESSAGE_TYPE get_rec_msg_type(struct PtpMetaData *ptpData)
{
    return ptpData->ptpMsgRec.header.transportSpecificMessageType.MessageType;
}

uint32_t get_rec_msg_correction(struct PtpMetaData *ptpData)
{
    uint64_t correction = get_8byte(ptpData->ptpMsgRec.header.correctionField, PTP_BIG_ENDIAN);
    return (uint32_t)(correction >> 16);
}

void get_rec_msg_timestamp(struct PtpMetaData *ptpData, struct PTP_TIMESTAMP *time)
{
    uint64_t temp = (*((uint64_t *)&ptpData->ptpMsgRec.body.ptpFollowUp.preciseOriginTimestamp.secondsField)) << 16;
    time->secondsField = get_8byte(temp, PTP_BIG_ENDIAN);
    time->nanosecondsField = get_4byte(ptpData->ptpMsgRec.body.ptpFollowUp.preciseOriginTimestamp.nanosecondsField, PTP_BIG_ENDIAN);
}

struct PtpMetaData *PTP_create()
{
    struct PtpMetaData *ptpData = calloc(1, sizeof(struct PtpMetaData));

    set_transport_specific(ptpData, 0);
    set_message_type(ptpData, DELAY_REQ);
    set_ptp_version(ptpData, PTP_VERSION_2);
    set_message_length(ptpData, 44);
    set_domain_number(ptpData, 0);
    set_flags(ptpData, UNICAST_FLAG);
    //set_correction(ptpData, uint64_t correction);
    set_port_id_oui(ptpData, 0x00FFFFFF);
    //set_port_id_uuid(ptpData, uint64_t uuid);
    set_port_id_port_number(ptpData, 1);
    set_sequence_id(ptpData, 0xBE);
    set_control_field(ptpData, 0x01);
    set_log_message_interval(ptpData, 0x7F);

    ptpData->pid.Kp = 0.187939135938621;    //0.5004;
    ptpData->pid.Ki = 0.000423853113175412; //0.0668; //0.0021
    ptpData->pid.Kd = 0;
    arm_pid_init_f32(&ptpData->pid, 0);
    ptpData->pid.state[2] = 0.0;

    set_rate(1.0);

    ptpData->comparisonAdd.secondsField = 0;
    ptpData->comparisonAdd.nanosecondsField = 10000000;
    setPTPEvent(PTP_Event_Handler, ptpData);

    ptpData->timeInit = false;

    GMAC_REGS->GMAC_TSL = 0;
    GMAC_REGS->GMAC_TSH = 0;
    GMAC_REGS->GMAC_TN = 0;

    ptpData->comparison.secondsField = 2;
    ptpData->comparison.nanosecondsField = 0;
    tsu_comp_set_time2(&ptpData->comparison);

    ptpData->udpPtpEvtClient = ConnectionData_create();

    ConnectionData_setup(ptpData->udpPtpEvtClient,
                         PTP_CRITICAL_EVENT_PORT,
                         CLIENT,
                         true,
                         TCPIP_UDP_SIGNAL_RX_DATA,
                         PTP_Packet_Received_Event_Handler,
                         ptpData,
                         "224.0.1.129");

    ptpData->udpPtpGnrClient = ConnectionData_create();
    ConnectionData_setup(ptpData->udpPtpGnrClient,
                         PTP_GENERAL_MESSAGE_PORT,
                         CLIENT,
                         true,
                         TCPIP_UDP_SIGNAL_RX_DATA,
                         PTP_Packet_Received_Event_Handler,
                         ptpData,
                         "224.0.1.129");

    StateMachine_create(&ptpData->stateMachine, &execute_ptp_state_machine, PTP_STATE_INIT, TRANS_COUNT, &trans, 50); //do cast in function
	TC2_TimerCallbackRegister(delay_request_interrupt, (uintptr_t) ptpData);																		
    return ptpData;
}

void PTP_destroy(struct PtpMetaData *ptp)
{
    //StateMachine_destroy(ptp->stateMachine);
    //ptp->stateMachine = NULL;
    if (ptp != NULL)
    {
        free(ptp);
    }
}

bool ptp_state_machine_execute(struct PtpMetaData *ptpData)
{
    return state_machine_execute(&ptpData->stateMachine);
};

static uint32_t init_eth(struct PtpMetaData *ptpData)
{
    if (eth_run_init(ptpData->udpPtpEvtClient) && eth_run_init(ptpData->udpPtpGnrClient))
    {
        while (discard_data(ptpData->udpPtpEvtClient));
        while (discard_data(ptpData->udpPtpGnrClient));
        Queue_clear(&ptpData->stateMachine);
        return PTP_STATE_IDLE;
    }
    else
    {
        return PTP_STATE_INIT;
    }
}

static uint32_t idle(struct PtpMetaData *ptpData)
{
    return PTP_STATE_IDLE;
}

static uint32_t decode_msg_evt(struct PtpMetaData *ptpData)
{
    EventObj event;
    read_data(ptpData->udpPtpEvtClient, (uint8_t *)&ptpData->ptpMsgRec, sizeof(struct PTP_MESSAGE));
    if ( get_state(&ptpData->stateMachine) != PTP_STATE_INIT)
    {
        if (get_rec_msg_type(ptpData) == SYNC)
        {
            event = PTP_EV_SYNC;
            Queue_event(&ptpData->stateMachine, &event);
        }
    }
    return get_state(&ptpData->stateMachine);
}

static uint32_t decode_msg_gnr(struct PtpMetaData *ptpData)
{
    EventObj event;
    read_data(ptpData->udpPtpGnrClient, (uint8_t *)&ptpData->ptpMsgRec, sizeof(struct PTP_MESSAGE));
    if (get_state(&ptpData->stateMachine) != PTP_STATE_INIT)
    {
        if (get_rec_msg_type(ptpData) == FOLLOW_UP)
        {
            event = PTP_EV_FOLLOW_UP;
            Queue_event(&ptpData->stateMachine, &event);
        }
        else if (get_rec_msg_type(ptpData) == DELAY_RESP)
        {
            event = PTP_EV_DELAY_RESPONSE;
            Queue_event(&ptpData->stateMachine, &event);
        }
    }
    return get_state(&ptpData->stateMachine);
}

static uint32_t sync(struct PtpMetaData *ptpData)
{
    tsu_get_time2(&ptpData->timestamps.t2, GMAC_REGS->GMAC_EFRSH, GMAC_REGS->GMAC_EFRSL, GMAC_REGS->GMAC_EFRN);
    ptpData->timestamps.s2.nanosecondsField = get_rec_msg_correction(ptpData);
    time_stamp_subtract(&ptpData->timestamps.t2, &ptpData->timestamps.s2, &ptpData->timestamps.t2);
    return PTP_STATE_WAIT_FOLLOW_UP;
}
																	   
void delay_request_interrupt(TC_TIMER_STATUS status, uintptr_t context)
{
    struct PtpMetaData *ptpData = (struct PtpMetaData *)context;
    EventObj event;
    event = PTP_EV_TIMER_INTERRUPT;
    Queue_event(&ptpData->stateMachine, &event);
} 
																


static uint32_t follow_up(struct PtpMetaData *ptpData)
{
    struct PTP_TIMESTAMP time;
    struct PTP_TIMESTAMP diff;
    

    tsu_get_time2(&time, GMAC_REGS->GMAC_TSH, GMAC_REGS->GMAC_TSL, GMAC_REGS->GMAC_TN);
    get_rec_msg_timestamp(ptpData, &ptpData->timestamps.t1);
    time_stamp_subtract(&time, &ptpData->timestamps.t1, &diff);
    if (diff.secondsField > (2 << PRES_EXTENSION))
    {
        tsu_set_time2(&ptpData->timestamps.t1);
        return PTP_STATE_IDLE;
    }
    else
    {
        TC2_TimerStart();
        
        if (TC2_Timer16bitPeriodGet() < 12000){
            return PTP_STATE_IDLE;
        }else{
            return PTP_STATE_WAIT_INTERRUPT;
        }
        
    }
}


static uint32_t send_delay_request(struct PtpMetaData *ptpData)
{
    transmit_data(ptpData->udpPtpEvtClient, (uint8_t *)&ptpData->ptpMsgTrans, sizeof(struct PTP_HEADER) + sizeof(struct PTP_DELAY_RESP));
    return PTP_STATE_WAIT_DELAY_REQ;
}															   

static uint32_t delay_req_TSU(struct PtpMetaData *ptpData)
{
    tsu_get_time2(&ptpData->timestamps.t3, GMAC_REGS->GMAC_EFTSH, GMAC_REGS->GMAC_EFTSL, GMAC_REGS->GMAC_EFTN);
    return PTP_STATE_WAIT_DELAY_RES;
}

static uint32_t delay_resp(struct PtpMetaData *ptpData)
{
    get_rec_msg_timestamp(ptpData, &ptpData->timestamps.t4);
    ptpData->timestamps.s4.nanosecondsField = get_rec_msg_correction(ptpData);
    time_stamp_subtract(&ptpData->timestamps.t4, &ptpData->timestamps.s4, &ptpData->timestamps.t4);
    run_PID(ptpData);

    global_ptp_data.t1s = ptpData->timestamps.t1.secondsField;
    global_ptp_data.t1ns = ptpData->timestamps.t1.nanosecondsField;

    global_ptp_data.t2s = ptpData->timestamps.t2.secondsField;
    global_ptp_data.t2ns = ptpData->timestamps.t2.nanosecondsField;

    global_ptp_data.t3s = ptpData->timestamps.t3.secondsField;
    global_ptp_data.t3ns = ptpData->timestamps.t3.nanosecondsField;

    global_ptp_data.t4s = ptpData->timestamps.t4.secondsField;
    global_ptp_data.t4ns = ptpData->timestamps.t4.nanosecondsField;

    global_ptp_data.T2 = ptpData->timestamps.s2.nanosecondsField;
    global_ptp_data.T4 = ptpData->timestamps.s4.nanosecondsField;

    global_ptp_data.offsetEst = (double)ptpData->time_offset_est;
    global_ptp_data.desiredRate = ptpData->desired_rate;
    global_ptp_data.setRate = ptpData->set_rate;

    ptpDataReady = true;
    return PTP_STATE_IDLE;
}

static float time_diff_float(struct PTP_TIMESTAMP t1, struct PTP_TIMESTAMP t2)
{
    struct PTP_TIMESTAMP temp;
    int64_t temp_ns;
    float time_diff_second;
    time_stamp_subtract(&t1, &t2, &temp);
    temp_ns = time_stamp_to_ns(temp);
    time_diff_second = temp_ns / BILLION_D;
    return time_diff_second;
}

//This only performs one way transfer synchronisation. Final implementation should use both transfers
static void run_PID(struct PtpMetaData *ptpData)
{
    ptpData->time_offset_est = time_diff_float(ptpData->timestamps.t1, ptpData->timestamps.t2);
    ptpData->desired_rate = 1.0 + ((double)arm_pid_f32(&ptpData->pid, ptpData->time_offset_est));
    ptpData->set_rate = set_rate(ptpData->desired_rate);
}

void set_transport_specific(struct PtpMetaData *ptpData, uint8_t transport)
{
    ptpData->ptpMsgTrans.header.transportSpecificMessageType.transportSpecific = transport;
}

void set_message_type(struct PtpMetaData *ptpData, enum PTP_MESSAGE_TYPE message)
{
    ptpData->ptpMsgTrans.header.transportSpecificMessageType.MessageType = message;
}

void set_ptp_version(struct PtpMetaData *ptpData, enum PTP_VERSION versionPTP)
{
    set_byte(&ptpData->ptpMsgTrans.header.versionPTP,versionPTP);
}

void set_message_length(struct PtpMetaData *ptpData, uint16_t length)
{
    set_2byte(&ptpData->ptpMsgTrans.header.messageLength, length, PTP_BIG_ENDIAN);
}

void set_domain_number(struct PtpMetaData *ptpData, uint8_t domain)
{
    set_byte(&ptpData->ptpMsgTrans.header.domainNumber,domain);
}

void set_flags(struct PtpMetaData *ptpData, enum PTP_HEADER_FLAGS flags)
{
    set_2byte(&ptpData->ptpMsgTrans.header.flagField,flags, PTP_BIG_ENDIAN);
}

void set_correction(struct PtpMetaData *ptpData, uint64_t correction)
{
    set_8byte(&ptpData->ptpMsgTrans.header.correctionField,correction, PTP_BIG_ENDIAN);
}

void set_port_id_oui(struct PtpMetaData *ptpData, uint32_t oui)
{
    set_4byte((uint32_t *)(&ptpData->ptpMsgTrans.header.sourcePortIdentity.clockID.OUI),oui, PTP_BIG_ENDIAN);
}

void set_port_id_uuid(struct PtpMetaData *ptpData, uint64_t uuid)
{
    set_8byte((uint64_t *)&ptpData->ptpMsgTrans.header.sourcePortIdentity.clockID.UUID, uuid, PTP_BIG_ENDIAN);
}

void set_port_id_port_number(struct PtpMetaData *ptpData, uint16_t portNumber)
{
    set_2byte(&ptpData->ptpMsgTrans.header.sourcePortIdentity.portNumber, portNumber, PTP_BIG_ENDIAN);
}

void set_sequence_id(struct PtpMetaData *ptpData, uint16_t sequenceId)
{
    set_2byte(&ptpData->ptpMsgTrans.header.sequenceId, sequenceId, PTP_BIG_ENDIAN);
}

void set_control_field(struct PtpMetaData *ptpData, uint8_t controlField)
{
    set_byte(&ptpData->ptpMsgTrans.header.controlField, controlField);
}

void set_log_message_interval(struct PtpMetaData *ptpData, uint8_t logMessageInterval)
{
    set_byte(&ptpData->ptpMsgTrans.header.logMessageInterval, logMessageInterval);
}