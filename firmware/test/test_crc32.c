#ifdef TEST

#include "unity.h"

#include "crc32.h"

uint8_t exFram[] = {0x00, 0x01, 0x02, 0x03, 0x04};
size_t n = sizeof(exFram)/sizeof(exFram[0]);
void setUp(void)
{
}

void tearDown(void)
{
}

void test_crc32(void)
{
    uint32_t out = crc32b(exFram, n);

    TEST_ASSERT_EQUAL_HEX32(0xD202EF8D, out);
}

#endif // TEST
