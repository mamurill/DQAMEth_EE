#ifdef TEST

#include "unity.h"

#include "state_machine.h"

enum StateMachineStates
{
    /* Application's state machine's initial state. */
    TEST_STATE_ERROR = ST_ERROR,
    TEST_STATE_NULL = ST_NULL,
    TEST_STATE_TERM = ST_TERM,
    TEST_STATE_ANY = ST_ANY,
    TEST_STATE_ONE,
    TEST_STATE_TWO,
    TEST_STATE_EVENT,
    TEST_STATE_THREE,
    TEST_STATE_FOUR,
    TEST_STATE_FIVE
};

enum StateMachineEvents
{
    TEST_EV_NONE = EV_NONE,
    TEST_EV_ANY = EV_ANY,
    TEST_EV_TEST_1,
    TEST_EV_TEST_2,
    TEST_EV_TEST_3,
    TEST_EV_TEST_4,
    TEST_EV_TEST_5
};

//Forward Declaration of State Machine Functions
static uint32_t f_1(void *data);
static uint32_t f_2(void *data);
static uint32_t f_3(void *data);
static uint32_t f_4(void *data);
static uint32_t f_5(void *data);
static uint32_t f_6(void *data);
static uint32_t f_7(void *data);

static struct TransitionObj trans[] = {
    {TEST_STATE_ONE, TEST_EV_NONE, &f_1},
    {TEST_STATE_ONE, TEST_EV_TEST_1, &f_2},
    {TEST_STATE_ONE, TEST_EV_TEST_2, &f_4},
    {TEST_STATE_ONE, TEST_EV_TEST_3, &f_6},
    {TEST_STATE_ONE, TEST_EV_TEST_5, &f_7},
    {TEST_STATE_TWO, EV_ANY, &f_3},
    {TEST_STATE_ANY, TEST_EV_TEST_4, &f_5},
    {TEST_STATE_ERROR, TEST_EV_NONE, &f_4}
};

static uint32_t f_1(void *data)
{
    return TEST_STATE_ONE;
}

static uint32_t f_2(void *data)
{
    return TEST_STATE_TWO;
}

static uint32_t f_3(void *data)
{
    return TEST_STATE_ONE;
}

static uint32_t f_4(void *data)
{
    return TEST_STATE_TERM;
}

static uint32_t f_5(void *data)
{
    return TEST_STATE_ERROR;
}

static uint32_t f_6(void *data)
{
    return TEST_STATE_THREE;
}

static uint32_t f_7(void *data)
{
    uint32_t test = *((uint32_t*) data);
    if(test == 1)
    {
        return TEST_STATE_FOUR;
    }
    if(test == 2)
    {
        return TEST_STATE_FIVE;
    }
}

static const uint32_t TRANS_COUNT = (sizeof(trans) / sizeof(*trans));

struct StateMachineMetaData *stateMachine;
void setUp(void)
{
    stateMachine = StateMachine_create(TEST_STATE_ONE, TRANS_COUNT, &trans, 10);
}

void tearDown(void)
{
    StateMachine_destroy(stateMachine);
    stateMachine = NULL;
}


void test_state_machine_noEvent(void)
{
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_ONE);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_ONE);
}

void test_state_machine_event(void)
{
    struct EventQueueObj event = {.event=TEST_EV_TEST_1};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_TWO);
    TEST_ASSERT_EQUAL(stateMachine->previous_state,TEST_STATE_ONE);
}

void test_state_machine_noEventnoAction(void)
{
   struct EventQueueObj event = {.event=TEST_EV_TEST_3};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    state_machine_execute(stateMachine, NULL);
    state_machine_execute(stateMachine, NULL);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_THREE);
    TEST_ASSERT_EQUAL(stateMachine->previous_state,TEST_STATE_ONE);
}

void test_state_machine_anyEvent(void)
{
    struct EventQueueObj event = {.event=TEST_EV_TEST_1};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);

    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_ONE);
    TEST_ASSERT_EQUAL(stateMachine->previous_state,TEST_STATE_TWO);

    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    event.event = TEST_EV_TEST_2;
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL(stateMachine->current_state,TEST_STATE_ONE);
    TEST_ASSERT_EQUAL(stateMachine->previous_state,TEST_STATE_TWO);
}

void test_state_machine_anyStatePart1(void)
{
    struct EventQueueObj event = {.event=TEST_EV_TEST_4};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_ERROR);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_ONE);
}

void test_state_machine_anyStatePart2(void)
{
    struct EventQueueObj event = {.event=TEST_EV_TEST_3};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_THREE);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_ONE);

    event.event = TEST_EV_TEST_4;
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_ERROR);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_THREE);
}

void test_state_machine_paramPassPart1(void)
{
    uint32_t data = 1;
    struct EventQueueObj event = {.event=TEST_EV_TEST_5};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, &data);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_FOUR);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_ONE);
}

void test_state_machine_paramPassPart2(void)
{
    uint32_t data = 2;
    struct EventQueueObj event = {.event=TEST_EV_TEST_5};
    Queue_event(stateMachine,&event);
    state_machine_execute(stateMachine, &data);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_FIVE);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_ONE);
}

void test_state_machine_terminate(void)
{
    bool terminated;
    struct EventQueueObj event = {.event=TEST_EV_TEST_2};
    Queue_event(stateMachine,&event);

    terminated = state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_FALSE(terminated);

    terminated = state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_TRUE(terminated);

    //Still terminated and does not execute even if an "ANY EVENT" occours
    event.event = TEST_EV_TEST_4;
    Queue_event(stateMachine,&event);
    terminated = state_machine_execute(stateMachine, NULL);
    TEST_ASSERT_TRUE(terminated);
    TEST_ASSERT_EQUAL_INT32(stateMachine->current_state,TEST_STATE_TERM);
    TEST_ASSERT_EQUAL_INT32(stateMachine->previous_state,TEST_STATE_ONE);
}

#endif // TEST
