#ifdef TEST

#include "unity.h"

#include "byte_swap.h"
#include "compile_options.h"
#include "agent_template.h"
#include "agents/agent_example1.h"
#include "protocol.c"

uint8_t testArrBigEnd[] = 
{
0x01,
0x02,
0x03,
0x04,
0x05,
0x06,
0x07,
0x08,
0x09,
0x0A,
0x0B,
0x0C,
0x0D,
0x0E,
0x0F,
0x10,
0x11,
0x12,
0x13,
0x14,
0x15,
0x16,
0x17,
0x18,
0x19,
0x1A,
0x1B,
0x1C,
0x1D,
0x1E,
0x1F,
0x20,
0x21,
0x22,
0x23,
0x24,
0x25,
0x26,
0x27,
0x28,
0x29,
0x2A,
0x2B,
0x2C,
0x2D,
0x2E,
0x2F,
0x30,
0x31,
0x32,
0x33,
0x34
};
struct ProtocolMetaData* protocolData;
void setUp(void)
{
    protocolData = FrameData_create(EXAMPLE_AGENT,1500);
}

void tearDown(void)
{
    FrameData_destroy(protocolData);
}

void test_protocol_objCreate(void)
{
    struct ProtocolMetaData* protocolData_test = FrameData_create(EXAMPLE_AGENT,1500);
    TEST_ASSERT_NOT_NULL(protocolData_test);
    TEST_ASSERT_TRUE(1);
}

void test_protocol_headerSize(void)
{
    TEST_ASSERT_EQUAL(2,sizeof(uint16_t));
    TEST_ASSERT_EQUAL(1,sizeof(struct FrameIdentifier));
    TEST_ASSERT_EQUAL(4,sizeof(uint32_t));
    TEST_ASSERT_EQUAL(8,sizeof(struct ProtocolTime));
    TEST_ASSERT_EQUAL(4,sizeof(struct Agent));

    TEST_ASSERT_EQUAL(19,sizeof(struct Header));
}

void test_protocol_objDataBigEndian(void)
{
    uint8_t* head = get_head(protocolData);
    uint16_t length = 0x0102;
    struct FrameIdentifier frameId = {.frameControl = 0x03, .frameType = 0x00};
    uint32_t frameCnt = 0x04050607;
    struct ProtocolTime time = {.timeSecs = 0x08090A0B, .timeNanosecs=0x0C0D0E0F};
    struct Agent agent = {.agentType = 0x1011, .version=0x12, .reserved=0x13} ;
    // enum DataId id = 0x14;
    // uint16_t status = 0x1516;
    // uint32_t index = 0x1718191A;
    // uint16_t slots = 0x1B1C;
    // uint32_t period = 0x1D1E1F20;
    // uint32_t data1S0 = 0x21222324;
    // uint32_t data2S0 = 0x25262728;
    // uint32_t data1S1 = 0x292A2B2C;
    // uint32_t data2S1 = 0x2D2E2F30;
    // crc32 crc = 0x31323334;
    set_length(protocolData, length);
    set_frameIdentifier(protocolData, frameId);
    set_frameCnt(protocolData, frameCnt);
    set_time(protocolData, time);
    set_agent(protocolData, agent);
    //set_dataID(protocolData, id);
    // set_status(protocolData, status);
    // set_index(protocolData, index);
    // set_slots(protocolData, slots);
    // set_period(protocolData, period);
    // set_crc(protocolData, crc);
    // set_EXAMPLE_AGENT_1(protocolData, 0, data1S0);
    // set_EXAMPLE_AGENT_2(protocolData, 0, data2S0);
    // set_EXAMPLE_AGENT_1(protocolData, 1, data1S1);
    // set_EXAMPLE_AGENT_2(protocolData, 1, data2S1);
    TEST_ASSERT_EQUAL_HEX8_ARRAY(testArrBigEnd, head, 19);
}

// void test_protocol_objDataLittleEndian(void)
// {
//     FrameData_destroy(protocolData);
//     protocolData = FrameData_create(EXAMPLE_AGENT,2, false);
//     uint8_t* head = get_head(protocolData);
//     uint16_t length = 0x0201;
//     struct FrameIdentifier frameId = {.frameControl = 0x03, .frameType = 0x00};
//     uint32_t frameCnt = 0x07060504;
//     struct ProtocolTime time = {.timeSecs = 0x0B0A0908, .timeNanosecs=0x0F0E0D0C};
//     struct Agent agent = {.agentType = 0x1110, .version=0x12, .reserved=0x13};
//     enum DataId id = 0x14;
//     uint16_t status = 0x1615;
//     uint32_t index = 0x1A191817;
//     uint16_t slots = 0x1C1B;
//     uint32_t period = 0x201F1E1D;
//     uint32_t data1S0 = 0x24232221;
//     uint32_t data2S0 = 0x28272625;
//     uint32_t data1S1 = 0x2C2B2A29;
//     uint32_t data2S1 = 0x302F2E2D;
//     crc32 crc = 0x34333231; 
//     set_length(protocolData, length);
//     set_frameIdentifier(protocolData, frameId);
//     set_frameCnt(protocolData, frameCnt);
//     set_time(protocolData, time);
//     set_agent(protocolData, agent);
//     set_dataID(protocolData, id);
//     set_status(protocolData, status);
//     set_index(protocolData, index);
//     set_slots(protocolData, slots);
//     set_period(protocolData, period);
//     set_crc(protocolData, crc);
//     set_EXAMPLE_AGENT_1(protocolData, 0, data1S0);
//     set_EXAMPLE_AGENT_2(protocolData, 0, data2S0);
//     set_EXAMPLE_AGENT_1(protocolData, 1, data1S1);
//     set_EXAMPLE_AGENT_2(protocolData, 1, data2S1);
//     TEST_ASSERT_EQUAL_HEX8_ARRAY(testArrBigEnd, head, 52);
// }

#endif // TEST
