#ifdef TEST

#include "unity.h"

#include "agent_example1.h"
#include "agent_example1.c"

void setUp(void)
{
    void* ptr = calloc(1, 200);
    struct agentExampleMetaData* agentMetaData = create_example_agent(ptr);
}

void tearDown(void)
{
}

void test_agent_example1_objCreate(void)
{
    void* ptr = calloc(1, 200);
    struct agentExampleMetaData* agentMetaData = create_example_agent(ptr);
    TEST_ASSERT_NOT_NULL(agentMetaData);
}

void test_agent_example1_objDataBigEndian(void)
{
    void* ptr = calloc(1, 200);
    struct agentExampleMetaData* agentMetaData = create_example_agent(ptr);
    TEST_ASSERT_NOT_NULL(agentMetaData);
}
#endif // TEST
