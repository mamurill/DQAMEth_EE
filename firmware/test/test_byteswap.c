#ifdef TEST

#include "unity.h"
#include "byte_swap.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_byteswap_check2Byte(void)
{
    uint16_t src = 0xFFFF;
    uint16_t dest = 0x0000;
    bswap16(&src, &dest);
    TEST_ASSERT_EQUAL_HEX16(0xFFFF, dest);

    src = 0xABCD;
    dest = 0x0000;
    bswap16(&src, &dest);
    TEST_ASSERT_EQUAL_HEX16(0xCDAB, dest);
}

void test_bitswap_check4Byte(void)
{
    uint32_t src = 0xFFFFFFFF;
    uint32_t dest = 0x0000;
    bswap32(&src, &dest);
    TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, dest);

    src = 0xABCDEF01;
    dest = 0x0000;
    bswap32(&src, &dest);
    TEST_ASSERT_EQUAL_HEX32(0x01EFCDAB, dest);
}

void test_bitswap_check8Byte(void)
{
    uint64_t src = 0xFFFFFFFFFFFFFFFF;
    uint64_t dest = 0x0000;
    //bswap64(&src, &dest);
    //TEST_ASSERT_EQUAL_HEX64(0xFFFFFFFFFFFFFFFF, dest);

    src = 0xABCDEF0123456789;
    dest = 0x0000;
    bswap64(&src, &dest);
    TEST_ASSERT_EQUAL_HEX64(0x8967452301EFCDAB, dest);
}

#endif // TEST
