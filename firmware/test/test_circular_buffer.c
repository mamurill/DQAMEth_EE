#ifdef TEST

#include "unity.h"

#include "fifo_buffer.h"

struct fifo_buffer* cb;
void setUp(void)
{
    cb = fifo_create(5,sizeof(uint32_t));
}

void tearDown(void)
{
    fifo_release(cb);
}

void test_circular_buffer_create(void)
{
    struct fifo_buffer* cb2 =  fifo_create(5, sizeof(uint32_t));
    TEST_ASSERT_NOT_NULL(cb2); //How else am I supposed to test this?
}

//Again, How am I supposed to test this?
// void test_circular_buffer_delete(void)
// {
// }

void test_circular_buffer_addPopItem(void)
{
    uint32_t data1 = 5;
    uint32_t data2 = 6;
    uint32_t data3 = 7;
    uint32_t data4 = 8;
    uint32_t data5 = 9;
    uint32_t out;
    fifo_write(cb, &data1);
    fifo_write(cb, &data2);
    fifo_write(cb, &data3);
    fifo_write(cb, &data4);
    fifo_write(cb, &data5);

    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data1,out); 
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data2,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data3,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data4,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data5,out);
    bool status = true;
    status = fifo_read(cb, &out);
    TEST_ASSERT_FALSE(status);

    fifo_write(cb, &data1);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data1,out);
}

void test_circular_buffer_addFull(void)
{
    uint32_t data1 = 5;
    uint32_t data2 = 6;
    uint32_t data3 = 7;
    uint32_t data4 = 8;
    uint32_t data5 = 9;
    uint32_t data6 = 10;
    uint32_t data7 = 11;
    uint32_t out;
    fifo_write(cb, &data1);
    fifo_write(cb, &data2);
    fifo_write(cb, &data3);
    fifo_write(cb, &data4);
    fifo_write(cb, &data5);
    fifo_write(cb, &data6);
    fifo_write(cb, &data7);
    
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data3,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data4,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data5,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data6,out);
    fifo_read(cb, &out);
    TEST_ASSERT_EQUAL(data7,out);

}

void test_circular_buffer_popEmpty(void)
{
    uint32_t out;
    bool success = false;
    success = fifo_read(cb, &out);
    TEST_ASSERT_FALSE(success);
}

void test_circular_buffer_browse(void)
{
    uint32_t data1 = 5;
    uint32_t data2 = 6;
    uint32_t data3 = 7;
    uint32_t data4 = 8;
    uint32_t data5 = 9;
    uint32_t out;
    fifo_browse(cb, &out, 1);

    fifo_write(cb, &data1);
    fifo_write(cb, &data2);
    fifo_write(cb, &data3);
    fifo_write(cb, &data4);
    fifo_write(cb, &data5);

    fifo_browse(cb, &out, 0);
    TEST_ASSERT_EQUAL(data1,out);

    fifo_browse(cb, &out, 1);
    TEST_ASSERT_EQUAL(data2,out);

    fifo_browse(cb, &out, 2);
    TEST_ASSERT_EQUAL(data3,out);

    fifo_browse(cb, &out, 3);
    TEST_ASSERT_EQUAL(data4,out);

    fifo_browse(cb, &out, 4);
    TEST_ASSERT_EQUAL(data5,out);


    fifo_browse(cb, &out, 5);
    TEST_ASSERT_EQUAL(data1,out);

    fifo_browse(cb, &out, 13);
    TEST_ASSERT_EQUAL(data4,out);
}

#endif // TEST
